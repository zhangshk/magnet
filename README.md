# magnet

#### 介绍
一款简单实用的分布式大数据处理框架，特点是零基础操作，支持批处理和流式处理。
<a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=06ecb7c18b0f2eec1114a35694602caf690b4ad5b071f83a47bd902162e84ef1"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="创玩大数据" title="创玩大数据"></a>
![输入图片说明](https://images.gitee.com/uploads/images/2020/0331/214445_a5d64ad6_612479.gif "magnet-03311.gif")
