package org.eocencle.magnet.session;

import org.eocencle.magnet.builder.Tag;

/**
 * 执行环境配置
 * @author: huan
 * @Date: 2020-01-12
 * @Description:
 */
public class RuntimeEnvironmentConfig {
    // 执行程序名称
    private String runEnv;
    // 执行程序版本
    private String version;

    public RuntimeEnvironmentConfig(ProjectConfig projectConfig) {
        this(projectConfig.getParameterInfo(Tag.MAGNET_CONTEXT).toString(),
                projectConfig.getParameterInfo(Tag.MAGNET_VERSION).toString());
    }

    public RuntimeEnvironmentConfig(String runEnv, String version) {
        this.runEnv = runEnv;
        this.version = version;
    }

    public String getRunEnv() {
        return runEnv;
    }

    public void setRunEnv(String runEnv) {
        this.runEnv = runEnv;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
