package org.eocencle.magnet.session;

import org.eocencle.magnet.builder.Tag;
import org.eocencle.magnet.mapping.DataSourceInfo;
import org.eocencle.magnet.mapping.WorkFlowInfo;
import org.eocencle.magnet.parsing.XNode;
import org.eocencle.magnet.util.StrictMap;

/**
 * 项目配置类
 * @author: huan
 * @Date: 2020-01-12
 * @Description:
 */
public class ProjectConfig {
    // 参数信息
    private StrictMap<Object> parameterInfo = new StrictMap<Object>("Parameter info");
    // 碎片信息
    private StrictMap<XNode> fragmentInfo = new StrictMap<XNode>("Fragment Info");
    // 数据源信息
    private StrictMap<DataSourceInfo> dataSourceInfo = new StrictMap<DataSourceInfo>("Data Source info");
    // 工作流信息
    private StrictMap<WorkFlowInfo> workFlowInfo = new StrictMap<WorkFlowInfo>("Work Flow info");

    public ProjectConfig() {
        this.initParameters();
    }

    /**
     * 初始化系统参数
     * @Author huan
     * @Date 2020-02-14
     * @Param []
     * @Return void
     * @Exception
     * @Description
     **/
    private void initParameters() {
        this.putParameterInfo(Tag.MAGNET_NAME, Tag.CONTEXT_APPNAME_DEFAULT);
        this.putParameterInfo(Tag.MAGNET_FILE_NAME, Tag.CONTEXT_APPNAME_DEFAULT);
        this.putParameterInfo(Tag.MAGNET_CONTEXT, Tag.CONTEXT_SPARK);
        this.putParameterInfo(Tag.MAGNET_VERSION, Tag.CONTEXT_VERSION_DEFALUT);
        this.putParameterInfo(Tag.MAGNET_ENV_MODE, Tag.ENV_MODE_LOCAL);
        this.putParameterInfo(Tag.MAGNET_PROCESS_MODE, Tag.PROCESS_MODE_BATCH);
        this.putParameterInfo(Tag.MAGNET_DURATION, Tag.STREAM_DEFAULT_DURATION);
        this.putParameterInfo(Tag.MAGNET_SQL_ENGINE, Tag.SQL_ENGINE_SPARK);
    }

    public void putParameterInfo(String key, Object value) {
        this.parameterInfo.put(key, value, true);
    }

    public StrictMap<Object> getParameterInfo() {
        return parameterInfo;
    }

    public Object getParameterInfo(String key) {
        return this.parameterInfo.get(key);
    }

    public void setParameterInfo(StrictMap<Object> parameterInfo) {
        this.parameterInfo = parameterInfo;
    }

    public void putFragmentInfo(String id, XNode node) {
        this.fragmentInfo.put(id, node);
    }

    public XNode getFragmentInfo(String id) {
        return this.fragmentInfo.get(id);
    }

    public StrictMap<XNode> getFragmentInfo() {
        return fragmentInfo;
    }

    public void setFragmentInfo(StrictMap<XNode> scriptInfo) {
        this.fragmentInfo = scriptInfo;
    }

    public StrictMap<DataSourceInfo> getDataSourceInfo() {
        return dataSourceInfo;
    }

    public void setDataSourceInfo(StrictMap<DataSourceInfo> dataSourceInfo) {
        this.dataSourceInfo = dataSourceInfo;
    }

    public void putDataSourceInfo(String id, DataSourceInfo dataSourceInfo) {
        this.dataSourceInfo.put(id, dataSourceInfo);
    }

    public StrictMap<WorkFlowInfo> getWorkFlowInfo() {
        return workFlowInfo;
    }

    public void setWorkFlowInfo(StrictMap<WorkFlowInfo> workFlowInfo) {
        this.workFlowInfo = workFlowInfo;
    }

    public void putWorkFlowInfo(String id, WorkFlowInfo workFlowInfo) {
        this.workFlowInfo.put(id, workFlowInfo, true);
    }
}
