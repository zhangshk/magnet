package org.eocencle.magnet.builder.xml;

import org.eocencle.magnet.builder.Tag;
import org.eocencle.magnet.mapping.DataSourceField;
import org.eocencle.magnet.mapping.TableInfo;
import org.eocencle.magnet.parsing.XNode;
import org.eocencle.magnet.parsing.XPathParser;
import org.eocencle.magnet.session.ProjectConfig;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * 文件引用数据源建构类
 * @author: huan
 * @Date: 2020-01-18
 * @Description:
 */
public class FileTableBuilder implements XMLParser {
    // 单例实体
    private static FileTableBuilder BUILDER = new FileTableBuilder();

    private FileTableBuilder() {

    }

    /**
     * 获取单例实体
     * @Author huan
     * @Date 2020-01-18
     * @Param []
     * @Return org.eocencle.magnet.builder.xml.FileTableBuilder
     * @Exception
     * @Description
     **/
    public static FileTableBuilder getInstance() {
        return BUILDER;
    }

    @Override
    public void parse(XNode node, ProjectConfig config) {
        try {
            String src = node.getStringAttribute(Tag.XML_ATTR_REF).replace(Tag.SRC_PREFIX_FILE, Tag.STRING_BLANK);
            XPathParser parser = new XPathParser(new FileInputStream(new File(src)));
            TableInfo table = this.getFileTable(parser.evalNode(Tag.XML_EL_TABLE));
            table.setId(node.getStringAttribute(Tag.XML_ATTR_ID));
            table.setAlias(node.getStringAttribute(Tag.XML_ATTR_ALIAS));
            config.putDataSourceInfo(table.getId(), table);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * 解析文件节点
     * @Author huan
     * @Date 2020-01-18
     * @Param [fileNode]
     * @Return org.eocencle.magnet.mapping.TableInfo
     * @Exception
     * @Description
     **/
    private TableInfo getFileTable(XNode fileNode) {
        TableInfo table = new TableInfo();
        table.setStyle(Tag.OUTPUT_STYLE_FILE);
        table.setSrc(fileNode.getStringAttribute(Tag.XML_ATTR_SRC));
        table.setFormat(fileNode.getStringAttribute(Tag.XML_ATTR_FORMAT, Tag.FILE_FORMAT_TEXTFILE));
        table.setSeparator(fileNode.getStringAttribute(Tag.XML_ATTR_SEPARATOR, Tag.SPLIT_INVISIBLE1));
        for (XNode childNode: fileNode.evalNode(Tag.XML_EL_FIELDS).getChildren()) {
            table.addField(new DataSourceField(childNode.getStringAttribute(Tag.XML_ATTR_NAME),
                    childNode.getStringAttribute(Tag.XML_ATTR_TYPE, Tag.TABLE_FIELD_TYPE_STRING),
                    childNode.getStringAttribute(Tag.XML_ATTR_PRECISION, null),
                    childNode.getStringAttribute(Tag.XML_ATTR_FORMAT, "yyyy-MM-dd HH:mm:ss")));
        }
        return table;
    }
}
