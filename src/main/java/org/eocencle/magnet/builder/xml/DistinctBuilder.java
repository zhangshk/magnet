package org.eocencle.magnet.builder.xml;

import org.eocencle.magnet.builder.Tag;
import org.eocencle.magnet.mapping.DistinctInfo;
import org.eocencle.magnet.parsing.XNode;
import org.eocencle.magnet.session.ProjectConfig;

import java.util.List;

/**
 * 去重建构类
 * @author: huan
 * @Date: 2020-03-15
 * @Description:
 */
public class DistinctBuilder implements XMLParser {
    // 单例实体
    private static DistinctBuilder BUILDER = new DistinctBuilder();

    private DistinctBuilder() {

    }

    /**
     * 获取单例实体
     * @Author huan
     * @Date 2020-01-18
     * @Param []
     * @Return org.eocencle.magnet.builder.xml.GroupBuilder
     * @Exception
     * @Description
     **/
    public static DistinctBuilder getInstance() {
        return BUILDER;
    }

    @Override
    public void parse(XNode node, ProjectConfig config) {
        this.parseElements(node.evalNodes(Tag.XML_EL_DISTINCT), config);
    }

    /**
     * 解析去重节点
     * @Author huan
     * @Date 2020-03-15
     * @Param [nodes, config]
     * @Return void
     * @Exception
     * @Description
     **/
    private void parseElements(List<XNode> nodes, ProjectConfig config) {
        if (null == nodes || 0 == nodes.size()) {
            return ;
        }

        DistinctInfo distinctInfo = null;
        for (XNode node: nodes) {
            distinctInfo = new DistinctInfo();
            distinctInfo.setId(node.getStringAttribute(Tag.XML_ATTR_ID));
            distinctInfo.setAlias(node.getStringAttribute(Tag.XML_ATTR_ALIAS));
            distinctInfo.setRef(node.getStringAttribute(Tag.XML_ATTR_REF));

            config.putWorkFlowInfo(distinctInfo.getId(), distinctInfo);
        }
    }
}
