package org.eocencle.magnet.builder.xml;

import org.eocencle.magnet.builder.Tag;
import org.eocencle.magnet.mapping.DataSourceField;
import org.eocencle.magnet.mapping.TableInfo;
import org.eocencle.magnet.parsing.XNode;
import org.eocencle.magnet.session.ProjectConfig;

/**
 * 默认数据源建构类
 * @author: huan
 * @Date: 2020-01-18
 * @Description:
 */
public class DefaultTableBuilder implements XMLParser {
    // 单例实体
    private static DefaultTableBuilder BUILDER = new DefaultTableBuilder();

    private DefaultTableBuilder() {

    }

    /**
     * 获取单例实体
     * @Author huan
     * @Date 2020-01-18
     * @Param []
     * @Return org.eocencle.magnet.builder.xml.DefaultTableBuilder
     * @Exception
     * @Description
     **/
    public static DefaultTableBuilder getInstance() {
        return BUILDER;
    }

    @Override
    public void parse(XNode node, ProjectConfig config) {
        TableInfo table = new TableInfo();
        // 添加表信息
        table.setId(node.getStringAttribute(Tag.XML_ATTR_ID));
        table.setAlias(node.getStringAttribute(Tag.XML_ATTR_ALIAS));
        table.setStyle(Tag.TABLE_STYLE_DEFAULT);
        table.setSrc(node.getStringAttribute(Tag.XML_ATTR_SRC));
        table.setFormat(node.getStringAttribute(Tag.XML_ATTR_FORMAT, Tag.FILE_FORMAT_TEXTFILE));
        table.setSeparator(node.getStringAttribute(Tag.XML_ATTR_SEPARATOR, Tag.SPLIT_INVISIBLE1));

        // 添加表字段信息
        XNode fieldsNode = node.evalNode(Tag.XML_EL_FIELDS);
        if (null != fieldsNode) {
            for (XNode childNode: fieldsNode.getChildren()) {
                table.addField(new DataSourceField(childNode.getStringAttribute(Tag.XML_ATTR_NAME),
                        childNode.getStringAttribute(Tag.XML_ATTR_TYPE, Tag.TABLE_FIELD_TYPE_STRING),
                        childNode.getStringAttribute(Tag.XML_ATTR_PRECISION, null),
                        childNode.getStringAttribute(Tag.XML_ATTR_FORMAT, "yyyy-MM-dd HH:mm:ss")));
            }
        }

        config.putDataSourceInfo(table.getId(), table);
    }
}
