package org.eocencle.magnet.builder.xml;

import org.eocencle.magnet.builder.Tag;
import org.eocencle.magnet.mapping.DataSourceField;
import org.eocencle.magnet.mapping.InfoParam;
import org.eocencle.magnet.mapping.TableInfo;
import org.eocencle.magnet.parsing.XNode;
import org.eocencle.magnet.session.ProjectConfig;

/**
 * 数据库数据源建构类
 * @author: huan
 * @Date: 2020-03-26
 * @Description:
 */
public class DataBaseTableBuilder implements XMLParser {
    // 单例实体
    private static DataBaseTableBuilder BUILDER = new DataBaseTableBuilder();

    private DataBaseTableBuilder() {

    }

    /**
     * 获取单例实体
     * @Author huan
     * @Date 2020-03-26
     * @Param []
     * @Return org.eocencle.magnet.builder.xml.DataBaseTableBuilder
     * @Exception
     * @Description
     **/
    public static DataBaseTableBuilder getInstance() {
        return BUILDER;
    }

    @Override
    public void parse(XNode node, ProjectConfig config) {
        TableInfo table = new TableInfo();
        // 添加表信息
        table.setId(node.getStringAttribute(Tag.XML_ATTR_ID));
        table.setAlias(node.getStringAttribute(Tag.XML_ATTR_ALIAS));
        table.setStyle(node.getStringAttribute(Tag.XML_ATTR_STYLE, Tag.TABLE_STYLE_DEFAULT));
        table.setSrc(node.getStringAttribute(Tag.XML_ATTR_SRC));

        // 添加流配置信息
        for (XNode childNode: node.evalNode(Tag.XML_EL_CONFIG).getChildren()) {
            // 获取参数
            table.putConfigParam(new InfoParam(childNode.getStringAttribute(Tag.XML_ATTR_KEY),
                    childNode.getStringAttribute(Tag.XML_ATTR_VALUE)));
        }

        // 添加表字段信息
        for (XNode childNode: node.evalNode(Tag.XML_EL_FIELDS).getChildren()) {
            table.addField(new DataSourceField(childNode.getStringAttribute(Tag.XML_ATTR_NAME),
                    childNode.getStringAttribute(Tag.XML_ATTR_TYPE, Tag.TABLE_FIELD_TYPE_STRING),
                    childNode.getStringAttribute(Tag.XML_ATTR_PRECISION, null),
                    childNode.getStringAttribute(Tag.XML_ATTR_FORMAT, "yyyy-MM-dd HH:mm:ss")));
        }

        config.putDataSourceInfo(table.getId(), table);
    }
}
