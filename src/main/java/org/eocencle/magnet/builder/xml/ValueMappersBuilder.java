package org.eocencle.magnet.builder.xml;

import org.eocencle.magnet.builder.Tag;
import org.eocencle.magnet.mapping.InfoParam;
import org.eocencle.magnet.mapping.ValueMappersInfo;
import org.eocencle.magnet.parsing.XNode;
import org.eocencle.magnet.session.ProjectConfig;

import java.util.List;

/**
 *
 * @author: huan
 * @Date: 2020-04-08
 * @Description:
 */
public class ValueMappersBuilder implements XMLParser {
    // 单例实体
    private static ValueMappersBuilder BUILDER = new ValueMappersBuilder();

    private ValueMappersBuilder() {

    }

    /**
     * 获取单例实体
     * @Author huan
     * @Date 2020-04-08
     * @Param []
     * @Return org.eocencle.magnet.builder.xml.ValueMappersBuilder
     * @Exception 
     * @Description
     **/
    public static ValueMappersBuilder getInstance() {
        return BUILDER;
    }

    @Override
    public void parse(XNode node, ProjectConfig config) {
        List<XNode> nodes = node.evalNodes(Tag.XML_EL_VALUEMAPPERS);
        this.parseElements(nodes, config);
    }

    private void parseElements(List<XNode> nodes, ProjectConfig config) {
        if (null == nodes) {
            return ;
        }

        ValueMappersInfo valueMappersInfo = null;
        ValueMappersInfo.ValueMapper valueMapper = null;
        InfoParam mapper = null;
        for (XNode node: nodes) {
            valueMappersInfo = new ValueMappersInfo();
            valueMappersInfo.setId(node.getStringAttribute(Tag.XML_ATTR_ID));
            valueMappersInfo.setAlias(node.getStringAttribute(Tag.XML_ATTR_ALIAS));
            valueMappersInfo.setRef(node.getStringAttribute(Tag.XML_ATTR_REF));

            List<XNode> children = node.getChildren();
            for (XNode child: children) {
                valueMapper = new ValueMappersInfo.ValueMapper();
                valueMapper.setField(child.getStringAttribute(Tag.XML_ATTR_FIELD));
                valueMapper.setTagField(child.getStringAttribute(Tag.XML_ATTR_TAGFIELD));
                valueMapper.setNonMatch(child.getStringAttribute(Tag.XML_ATTR_NONMATCH));

                List<XNode> ms = child.getChildren();
                for (XNode m: ms) {
                    mapper = new InfoParam(m.getStringAttribute(Tag.XML_ATTR_SOURCE),
                            m.getStringAttribute(Tag.XML_ATTR_TARGET));

                    valueMapper.putValMapper(mapper);
                }

                valueMappersInfo.addValueMapper(valueMapper);
            }

            config.putWorkFlowInfo(valueMappersInfo.getId(), valueMappersInfo);
        }
    }
}
