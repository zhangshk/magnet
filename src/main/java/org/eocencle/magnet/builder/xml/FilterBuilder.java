package org.eocencle.magnet.builder.xml;

import org.eocencle.magnet.builder.Tag;
import org.eocencle.magnet.mapping.FilterInfo;
import org.eocencle.magnet.parsing.XNode;
import org.eocencle.magnet.session.ProjectConfig;

import java.util.List;

/**
 * 过滤建构类
 * @author: huan
 * @Date: 2020-03-13
 * @Description:
 */
public class FilterBuilder implements XMLParser {
    // 单例实体
    private static FilterBuilder BUILDER = new FilterBuilder();

    private FilterBuilder() {

    }

    /**
     * 获取单例实体
     * @Author huan
     * @Date 2020-03-13
     * @Param []
     * @Return org.eocencle.magnet.builder.xml.FilterBuilder
     * @Exception
     * @Description
     **/
    public static FilterBuilder getInstance() {
        return BUILDER;
    }

    @Override
    public void parse(XNode node, ProjectConfig config) {
        this.parseElements(node.evalNodes(Tag.XML_EL_FILTER), config);
    }

    /**
     * 解析元素
     * @Author huan
     * @Date 2020-03-13
     * @Param [nodes, config]
     * @Return void
     * @Exception
     * @Description
     **/
    private void parseElements(List<XNode> nodes, ProjectConfig config) {
        if (null == nodes || 0 == nodes.size()) {
            return ;
        }

        FilterInfo filterInfo = null;
        List<XNode> conds = null;
        String tagName = null;
        FilterInfo.FilterField filterField = null;
        for (XNode node: nodes) {
            filterInfo = new FilterInfo();
            filterInfo.setId(node.getStringAttribute(Tag.XML_ATTR_ID));
            filterInfo.setAlias(node.getStringAttribute(Tag.XML_ATTR_ALIAS));
            filterInfo.setRef(node.getStringAttribute(Tag.XML_ATTR_REF));
            conds = node.evalNode(Tag.XML_EL_CONDITIONS).getChildren();
            for (XNode cond: conds) {
                filterField = new FilterInfo.FilterField();
                tagName = cond.getName().toUpperCase();
                if (tagName.startsWith(Tag.FILTER_JOIN_AND)) {
                    filterField.setJoin(Tag.FILTER_JOIN_AND);
                    filterField.setType(tagName.substring(Tag.FILTER_JOIN_AND.length()));
                } else if (tagName.startsWith(Tag.FILTER_JOIN_OR)) {
                    filterField.setJoin(Tag.FILTER_JOIN_OR);
                    filterField.setType(tagName.substring(Tag.FILTER_JOIN_OR.length()));
                }
                filterField.setField(cond.getStringAttribute(Tag.XML_ATTR_FIELD));
                filterField.setValue(cond.getStringAttribute(Tag.XML_ATTR_VALUE));
                filterField.setStart(cond.getStringAttribute(Tag.XML_ATTR_START));
                filterField.setEnd(cond.getStringAttribute(Tag.XML_ATTR_END));

                filterInfo.addFilterFields(filterField);
            }

            config.putWorkFlowInfo(filterInfo.getId(), filterInfo);
        }
    }
}
