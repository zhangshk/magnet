package org.eocencle.magnet.builder.xml.factory;

import org.eocencle.magnet.builder.Tag;
import org.eocencle.magnet.builder.xml.*;

/**
 * 数据源工厂类
 * @author: huan
 * @Date: 2020-01-18
 * @Description:
 */
public class TableSourceFactory {
    /**
     * 获取表源实例
     * @Author huan
     * @Date 2020-01-18
     * @Param [style]
     * @Return org.eocencle.magnet.builder.xml.XMLParser
     * @Exception
     * @Description
     **/
    public static XMLParser getTableSourceBuilder(String style) {
        if (Tag.TABLE_STYLE_DEFAULT.equalsIgnoreCase(style)) {
            return DefaultTableBuilder.getInstance();
        } else if (Tag.TABLE_STYLE_FILE.equalsIgnoreCase(style)) {
            return FileTableBuilder.getInstance();
        } else if (Tag.TABLE_STYLE_HTTP.equalsIgnoreCase(style)) {
            return HttpTableBuilder.getInstance();
        } else if (Tag.TABLE_STYLE_DATABASE.equalsIgnoreCase(style)) {
            return DataBaseTableBuilder.getInstance();
        }
        throw new RuntimeException("数据源src配置有误！");
    }

}
