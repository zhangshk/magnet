package org.eocencle.magnet.builder.xml;

import org.eocencle.magnet.builder.Tag;
import org.eocencle.magnet.parsing.XNode;
import org.eocencle.magnet.session.ProjectConfig;

/**
 * 参数建构类
 * @author: huan
 * @Date: 2020-01-14
 * @Description:
 */
public class ParameterBuilder implements XMLParser {
    // 单例实体
    private static ParameterBuilder BUILDER = new ParameterBuilder();

    private ParameterBuilder() {

    }

    /**
     * 获取单例实体
     * @Author huan
     * @Date 2020-01-18
     * @Param []
     * @Return org.eocencle.magnet.builder.xml.ParameterBuilder
     * @Exception
     * @Description
     **/
    public static ParameterBuilder getInstance() {
        return BUILDER;
    }

    @Override
    public void parse(XNode node, ProjectConfig config) {
        // 获取变量参数标签
        XNode currNode = node.evalNode(Tag.XML_EL_PARAMETER);
        if (null == currNode) return ;
        VariableBuilder.getInstance().parse(currNode, config);
    }
}
