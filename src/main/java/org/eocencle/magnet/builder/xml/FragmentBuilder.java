package org.eocencle.magnet.builder.xml;

import org.eocencle.magnet.builder.Tag;
import org.eocencle.magnet.parsing.XNode;
import org.eocencle.magnet.session.ProjectConfig;

/**
 * 碎片建构类
 * @author: huan
 * @Date: 2020-03-07
 * @Description:
 */
public class FragmentBuilder implements XMLParser {
    // 单例实体
    private static FragmentBuilder BUILDER = new FragmentBuilder();

    private FragmentBuilder() {

    }

    /**
     * 获取单例实体
     * @Author huan
     * @Date 2020-03-07
     * @Param []
     * @Return org.eocencle.magnet.builder.xml.FragmentBuilder
     * @Exception
     * @Description
     **/
    public static FragmentBuilder getInstance() {
        return BUILDER;
    }

    @Override
    public void parse(XNode node, ProjectConfig config) {
        ScriptBuilder builder = ScriptBuilder.getInstance();
        builder.parse(node.evalNode(Tag.XML_EL_FRAGMENT), config);
    }
}
