package org.eocencle.magnet.builder.xml;

import org.eocencle.magnet.builder.Tag;
import org.eocencle.magnet.parsing.XNode;
import org.eocencle.magnet.session.ProjectConfig;

import java.util.List;

/**
 * 工作流建构类
 * @author: huan
 * @Date: 2020-01-14
 * @Description:
 */
public class WorkFlowBuilder implements XMLParser {
    // 单例实体
    private static WorkFlowBuilder BUILDER = new WorkFlowBuilder();

    private WorkFlowBuilder() {

    }

    /**
     * 获取单例实体
     * @Author huan
     * @Date 2020-02-01
     * @Param []
     * @Return org.eocencle.magnet.builder.xml.WorkFlowBuilder
     * @Exception
     * @Description
     **/
    public static WorkFlowBuilder getInstance() {
        return BUILDER;
    }

    @Override
    public void parse(XNode node, ProjectConfig config) {
        XNode workflowNode = node.evalNode(Tag.XML_EL_WORKFLOW);
        this.getElementsOrder(workflowNode.getChildren(), config);
        this.parseSQLElements(workflowNode, config);
        this.parseBranchElements(workflowNode, config);
        this.parseOutputElements(workflowNode, config);
        this.parseGroupElements(workflowNode, config);
        this.parseFilterElements(workflowNode, config);
        this.parseDistinctElements(workflowNode, config);
        this.parseOrderElements(workflowNode, config);
        this.parseUnionElements(workflowNode, config);
        this.parseJoinElements(workflowNode, config);
        this.parseSchemaElements(workflowNode, config);
        this.parseValueMappersElements(workflowNode, config);
    }

    /**
     * 获取工作流元素顺序
     * @Author huan
     * @Date 2020-02-01
     * @Param [nodes, config]
     * @Return void
     * @Exception
     * @Description
     **/
    private void getElementsOrder(List<XNode> nodes, ProjectConfig config) {
        for (XNode node: nodes) {
            config.putWorkFlowInfo(node.getStringAttribute(Tag.XML_ATTR_ID), null);
        }
    }

    /**
     * 解析SQL元素
     * @Author huan
     * @Date 2020-02-01
     * @Param [node, config]
     * @Return void
     * @Exception
     * @Description
     **/
    private void parseSQLElements(XNode node, ProjectConfig config) {
        SQLBuilder builder = SQLBuilder.getInstance();
        builder.parse(node, config);
    }

    /**
     * 解析分支元素
     * @Author huan
     * @Date 2020-02-01
     * @Param [node, config]
     * @Return void
     * @Exception
     * @Description
     **/
    private void parseBranchElements(XNode node, ProjectConfig config) {
        BranchBuilder builder = BranchBuilder.getInstance();
        builder.parse(node, config);
    }

    /**
     * 解析输出元素
     * @Author huan
     * @Date 2020-02-01
     * @Param [node, config]
     * @Return void
     * @Exception
     * @Description
     **/
    private void parseOutputElements(XNode node, ProjectConfig config) {
        OutputBuilder builder = OutputBuilder.getInstance();
        builder.parse(node, config);
    }

    /**
     * 解析分组元素
     * @Author huan
     * @Date 2020-03-10
     * @Param [node, config]
     * @Return void
     * @Exception
     * @Description
     **/
    private void parseGroupElements(XNode node, ProjectConfig config) {
        GroupBuilder builder = GroupBuilder.getInstance();
        builder.parse(node, config);
    }

    /**
     * 解析过滤元素
     * @Author huan
     * @Date 2020-03-13
     * @Param [node, config]
     * @Return void
     * @Exception
     * @Description
     **/
    private void parseFilterElements(XNode node, ProjectConfig config) {
        FilterBuilder builder = FilterBuilder.getInstance();
        builder.parse(node, config);
    }

    /**
     * 解析去重元素
     * @Author huan
     * @Date 2020-03-15
     * @Param [node, config]
     * @Return void
     * @Exception
     * @Description
     **/
    private void parseDistinctElements(XNode node, ProjectConfig config) {
        DistinctBuilder builder = DistinctBuilder.getInstance();
        builder.parse(node, config);
    }

    /**
     * 解析排序元素
     * @Author huan
     * @Date 2020-03-16
     * @Param [node, config]
     * @Return void
     * @Exception
     * @Description
     **/
    private void parseOrderElements(XNode node, ProjectConfig config) {
        OrderBuilder builder = OrderBuilder.getInstance();
        builder.parse(node, config);
    }

    /**
     * 解析合并元素
     * @Author huan
     * @Date 2020-04-03
     * @Param [node, config]
     * @Return void
     * @Exception
     * @Description
     **/
    private void parseUnionElements(XNode node, ProjectConfig config) {
        UnionBuilder builder = UnionBuilder.getInstance();
        builder.parse(node, config);
    }

    /**
     * 解析关联元素
     * @Author huan
     * @Date 2020-04-04
     * @Param [node, config]
     * @Return void
     * @Exception
     * @Description
     **/
    private void parseJoinElements(XNode node, ProjectConfig config) {
        JoinBuilder builder = JoinBuilder.getInstance();
        builder.parse(node, config);
    }

    /**
     * 解析Schema元素
     * @Author huan
     * @Date 2020-04-06
     * @Param [node, config]
     * @Return void
     * @Exception
     * @Description
     **/
    private void parseSchemaElements(XNode node, ProjectConfig config) {
        SchemaBuilder builder = SchemaBuilder.getInstance();
        builder.parse(node, config);
    }

    /**
     * 解析值映射元素
     * @Author huan
     * @Date 2020-04-08
     * @Param [node, config]
     * @Return void
     * @Exception
     * @Description
     **/
    private void parseValueMappersElements(XNode node, ProjectConfig config) {
        ValueMappersBuilder builder = ValueMappersBuilder.getInstance();
        builder.parse(node, config);
    }
}
