package org.eocencle.magnet.builder.xml;

import org.eocencle.magnet.builder.Tag;
import org.eocencle.magnet.mapping.GroupInfo;
import org.eocencle.magnet.parsing.XNode;
import org.eocencle.magnet.session.ProjectConfig;

import java.util.List;

import static org.eocencle.magnet.builder.Tag.XML_EL_GROUP;

/**
 * 分组建构类
 * @author: huan
 * @Date: 2020-01-18
 * @Description:
 */
public class GroupBuilder implements XMLParser {
    // 单例实体
    private static GroupBuilder BUILDER = new GroupBuilder();

    private GroupBuilder() {

    }

    /**
     * 获取单例实体
     * @Author huan
     * @Date 2020-01-18
     * @Param []
     * @Return org.eocencle.magnet.builder.xml.GroupBuilder
     * @Exception
     * @Description
     **/
    public static GroupBuilder getInstance() {
        return BUILDER;
    }

    @Override
    public void parse(XNode node, ProjectConfig config) {
        this.parseElements(node.evalNodes(XML_EL_GROUP), config);
    }

    /**
     * 解析分组节点
     * @Author huan
     * @Date 2020-01-18
     * @Param [nodes]
     * @Return void
     * @Exception
     * @Description
     **/
    private void parseElements(List<XNode> nodes, ProjectConfig config) {
        if (null == nodes || 0 == nodes.size()) {
            return ;
        }

        GroupInfo groupInfo = null;
        String[] fields = null, key = null, orders = null;
        String field = null;
        for (XNode node: nodes) {
            groupInfo = new GroupInfo();
            groupInfo.setId(node.getStringAttribute(Tag.XML_ATTR_ID));
            groupInfo.setAlias(node.getStringAttribute(Tag.XML_ATTR_ALIAS));
            groupInfo.setRef(node.getStringAttribute(Tag.XML_ATTR_REF));
            field = node.getStringAttribute(Tag.XML_ATTR_FIELD);
            fields = field.trim().split(Tag.SPLIT_COMMA);
            for (String val: fields) {
                groupInfo.addGroupField(new GroupInfo.GroupField(val.trim()));
            }

            orders = node.getStringAttribute(Tag.XML_ATTR_ORDER).split(Tag.SPLIT_COMMA);
            for (String order: orders) {
                key = order.trim().split(Tag.SPLIT_BLANK);
                if (2 == key.length) {
                    groupInfo.addOrderField(new GroupInfo.OrderField(key[0].trim(), key[1].trim()));
                } else {
                    groupInfo.addOrderField(new GroupInfo.OrderField(key[0].trim()));
                }
            }

            groupInfo.setRownumField(node.getStringAttribute(Tag.XML_ATTR_ROWNUM));

            config.putWorkFlowInfo(groupInfo.getId(), groupInfo);
        }
    }
}
