package org.eocencle.magnet.builder.xml;

import org.eocencle.magnet.builder.Tag;
import org.eocencle.magnet.builder.xml.factory.TableSourceFactory;
import org.eocencle.magnet.parsing.XNode;
import org.eocencle.magnet.session.ProjectConfig;

import java.util.List;

/**
 * 数据源建构类
 * @author: huan
 * @Date: 2020-01-14
 * @Description:
 */
public class DataSourceBuilder implements XMLParser {
    // 单例实体
    private static DataSourceBuilder BUILDER = new DataSourceBuilder();

    private DataSourceBuilder() {

    }

    /**
     * 获取单例实体
     * @Author huan
     * @Date 2020-02-01
     * @Param []
     * @Return org.eocencle.magnet.builder.xml.DataSourceBuilder
     * @Exception
     * @Description
     **/
    public static DataSourceBuilder getInstance() {
        return BUILDER;
    }

    @Override
    public void parse(XNode node, ProjectConfig config) {
        XNode datasourceNode = node.evalNode(Tag.XML_EL_DATASOURCE);
        this.parseTableElements(datasourceNode.evalNodes(Tag.XML_EL_TABLE), config);
        StreamBuilder.getInstance().parse(datasourceNode, config);
    }

    /**
     * 解析表元素
     * @Author huan
     * @Date 2020-02-16
     * @Param [nodes, config]
     * @Return void
     * @Exception
     * @Description
     **/
    private void parseTableElements(List<XNode> nodes, ProjectConfig config) {
        String style = null;
        for (XNode node: nodes) {
            style = node.getStringAttribute(Tag.XML_ATTR_STYLE, Tag.TABLE_STYLE_DEFAULT);
            TableSourceFactory.getTableSourceBuilder(style).parse(node, config);
        }
    }
}
