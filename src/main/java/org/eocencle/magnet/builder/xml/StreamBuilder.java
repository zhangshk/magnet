package org.eocencle.magnet.builder.xml;

import org.eocencle.magnet.builder.Tag;
import org.eocencle.magnet.mapping.InfoParam;
import org.eocencle.magnet.mapping.StreamInfo;
import org.eocencle.magnet.mapping.DataSourceField;
import org.eocencle.magnet.parsing.XNode;
import org.eocencle.magnet.session.ProjectConfig;

import java.util.List;

/**
 * 流信息建构类
 * @author: huan
 * @Date: 2020-02-16
 * @Description:
 */
public class StreamBuilder implements XMLParser {
    // 单例实体
    private static StreamBuilder BUILDER = new StreamBuilder();

    private StreamBuilder() {

    }

    /**
     * 获取单例实体
     * @Author huan
     * @Date 2020-02-16
     * @Param []
     * @Return org.eocencle.magnet.builder.xml.StreamBuilder
     * @Exception
     * @Description
     **/
    public static StreamBuilder getInstance() {
        return BUILDER;
    }

    @Override
    public void parse(XNode node, ProjectConfig config) {
        List<XNode> nodes = node.evalNodes(Tag.XML_EL_STREAM);
        for (XNode n: nodes) {
            this.parseStreamEleme(n, config);
        }
    }

    private void parseStreamEleme(XNode node, ProjectConfig config) {
        StreamInfo stream = new StreamInfo();
        // 添加流信息
        stream.setId(node.getStringAttribute(Tag.XML_ATTR_ID));
        stream.setAlias(node.getStringAttribute(Tag.XML_ATTR_ALIAS));
        stream.setTopics(node.getStringAttribute(Tag.XML_ATTR_TOPICS));
        stream.setFormat(node.getStringAttribute(Tag.XML_ATTR_FORMAT, Tag.FILE_FORMAT_TEXTFILE));
        stream.setSeparator(node.getStringAttribute(Tag.XML_ATTR_SEPARATOR, Tag.SPLIT_INVISIBLE1));

        // 添加流配置信息
        for (XNode childNode: node.evalNode(Tag.XML_EL_CONFIG).getChildren()) {
            // 获取参数
            stream.putKafkaConfig(new InfoParam(childNode.getStringAttribute(Tag.XML_ATTR_KEY),
                    childNode.getStringAttribute(Tag.XML_ATTR_VALUE)));
        }

        // 添加流字段信息
        for (XNode childNode: node.evalNode(Tag.XML_EL_FIELDS).getChildren()) {
            stream.addField(new DataSourceField(childNode.getStringAttribute(Tag.XML_ATTR_NAME),
                    childNode.getStringAttribute(Tag.XML_ATTR_TYPE, Tag.TABLE_FIELD_TYPE_STRING),
                    childNode.getStringAttribute(Tag.XML_ATTR_PRECISION, null),
                    childNode.getStringAttribute(Tag.XML_ATTR_FORMAT, "yyyy-MM-dd HH:mm:ss")));
        }

        config.putDataSourceInfo(stream.getId(), stream);
    }
}
