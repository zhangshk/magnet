package org.eocencle.magnet.builder.xml;

import org.eocencle.magnet.builder.Tag;
import org.eocencle.magnet.parsing.XNode;
import org.eocencle.magnet.session.ProjectConfig;
import org.eocencle.magnet.util.StrictMap;

import java.util.ArrayList;
import java.util.List;

/**
 * 变量建构类
 * @author: huan
 * @Date: 2020-01-17
 * @Description:
 */
public class VariableBuilder implements XMLParser {
    // 单例实体
    private static VariableBuilder BUILDER = new VariableBuilder();

    private VariableBuilder() {

    }

    /**
     * 获取单例实体
     * @Author huan
     * @Date 2020-01-18
     * @Param []
     * @Return org.eocencle.magnet.builder.xml.VariableBuilder
     * @Exception
     * @Description
     **/
    public static VariableBuilder getInstance() {
        return BUILDER;
    }

    @Override
    public void parse(XNode node, ProjectConfig config) {
        List<XNode> nodes = node.getChildren();
        List<XNode> child = null;
        for (XNode n: nodes) {
            // 获取参数
            child = n.getChildren();
            if (child.isEmpty()) {
                config.putParameterInfo(n.getStringAttribute(Tag.XML_ATTR_KEY),
                        n.getStringAttribute(Tag.XML_ATTR_VALUE));
            } else if (Tag.XML_EL_LIST.equals(child.get(0).getName())) {
                config.putParameterInfo(n.getStringAttribute(Tag.XML_ATTR_KEY),
                        this.parseList(child.get(0)));
            } else if (Tag.XML_EL_MAP.equals(child.get(0).getName())) {
                config.putParameterInfo(n.getStringAttribute(Tag.XML_ATTR_KEY),
                        this.parseMap(child.get(0)));
            }
        }
    }

    /**
     * 解析list变量
     * @Author huan
     * @Date 2020-03-07
     * @Param [node]
     * @Return java.util.List<java.lang.String>
     * @Exception
     * @Description
     **/
    private List<String> parseList(XNode node) {
        List<String> list = new ArrayList<>();
        for (XNode n: node.getChildren()) {
            list.add(n.getStringBody());
        }
        return list;
    }

    /**
     * 解析map变量
     * @Author huan
     * @Date 2020-03-07
     * @Param [node]
     * @Return org.eocencle.magnet.util.StrictMap<java.lang.String>
     * @Exception
     * @Description
     **/
    private StrictMap<String> parseMap(XNode node) {
        StrictMap<String> map = new StrictMap<String>("Custom map");
        for (XNode n: node.getChildren()) {
            map.put(n.getStringAttribute(Tag.XML_ATTR_KEY), n.getStringAttribute(Tag.XML_ATTR_VALUE));
        }
        return map;
    }
}
