package org.eocencle.magnet.builder.xml;

import org.eocencle.magnet.builder.ProjectConfigBuilder;
import org.eocencle.magnet.builder.Tag;
import org.eocencle.magnet.parsing.XNode;
import org.eocencle.magnet.parsing.XPathParser;
import org.eocencle.magnet.session.ProjectConfig;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * XML配置建构者类
 * @author: huan
 * @Date: 2020-01-13
 * @Description:
 */
public class XMLConfigurationBuilder extends ProjectConfigBuilder {
    // 节点
    private XNode node;

    public XMLConfigurationBuilder(ProjectConfig config, String filePath) throws FileNotFoundException {
        this(config, new FileInputStream(filePath));
    }

    public XMLConfigurationBuilder(ProjectConfig config, InputStream stream) {
        this(config, new XPathParser(stream));
    }

    public XMLConfigurationBuilder(ProjectConfig config, XPathParser parser) {
        this(config, parser.evalNode(Tag.XML_EL_PROJECT));
    }

    public XMLConfigurationBuilder(ProjectConfig config, XNode node) {
        super(config);
        this.node = node;
    }

    @Override
    public void build() {
        this.parseFragment();
        super.build();
    }

    /**
     * 解析SQL碎片
     * @Author huan
     * @Date 2020-03-07
     * @Param []
     * @Return void
     * @Exception
     * @Description
     **/
    public void parseFragment() {
        FragmentBuilder builder = FragmentBuilder.getInstance();
        builder.parse(this.node, this.getConfig());
    }

    @Override
    public void parseParameter() {
        ParameterBuilder builder = ParameterBuilder.getInstance();
        builder.parse(this.node, this.getConfig());
    }

    @Override
    public void parseDataSource() {
        DataSourceBuilder builder = DataSourceBuilder.getInstance();
        builder.parse(this.node, this.getConfig());
    }

    @Override
    public void parseWorkFlow() {
        WorkFlowBuilder builder = WorkFlowBuilder.getInstance();
        builder.parse(this.node, this.getConfig());
    }
}
