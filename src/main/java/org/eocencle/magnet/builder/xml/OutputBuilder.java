package org.eocencle.magnet.builder.xml;

import org.eocencle.magnet.builder.Tag;
import org.eocencle.magnet.mapping.InfoParam;
import org.eocencle.magnet.mapping.OutputInfo;
import org.eocencle.magnet.parsing.XNode;
import org.eocencle.magnet.session.ProjectConfig;

import java.util.List;

/**
 * 输出建构类
 * @author: huan
 * @Date: 2020-01-19
 * @Description:
 */
public class OutputBuilder implements XMLParser {
    // 单例实体
    private static OutputBuilder BUILDER = new OutputBuilder();

    private OutputBuilder() {

    }

    /**
     * 获取单例实体
     * @Author huan
     * @Date 2020-01-19
     * @Param []
     * @Return org.eocencle.magnet.builder.xml.OutputBuilder
     * @Exception
     * @Description
     **/
    public static OutputBuilder getInstance() {
        return BUILDER;
    }

    @Override
    public void parse(XNode node, ProjectConfig config) {
        this.parseElements(node.evalNodes(Tag.XML_EL_OUTPUT), config);
    }

    /**
     * 解析输出节点
     * @Author huan
     * @Date 2020-01-19
     * @Param [nodes, config]
     * @Return void
     * @Exception
     * @Description
     **/
    private void parseElements(List<XNode> nodes, ProjectConfig config) {
        if (null == nodes || 0 == nodes.size()) {
            return ;
        }

        OutputInfo outputInfo = null;
        for (XNode node: nodes) {
            outputInfo = new OutputInfo();
            outputInfo.setId(node.getStringAttribute(Tag.XML_ATTR_ID));
            outputInfo.setRef(node.getStringAttribute(Tag.XML_ATTR_REF));
            outputInfo.setStyle(node.getStringAttribute(Tag.XML_ATTR_STYLE, Tag.OUTPUT_STYLE_FILE));
            outputInfo.setTarget(node.getStringAttribute(Tag.XML_ATTR_TARGET));
            outputInfo.setType(node.getStringAttribute(Tag.XML_ATTR_TYPE, Tag.OUTPUT_TYPE_CREATE));
            outputInfo.setCompress(node.getStringAttribute(Tag.XML_ATTR_COMPRESS, Tag.COMPRESS_NONE));
            outputInfo.setSeparator(node.getStringAttribute(Tag.XML_ATTR_SEPARATOR, Tag.SPLIT_INVISIBLE1));

            XNode configNode = node.evalNode(Tag.XML_EL_CONFIG);
            if (null != configNode) {
                List<XNode> params = configNode.getChildren();
                for (XNode cnode: params) {
                    outputInfo.putOutputConfig(new InfoParam(cnode.getStringAttribute(Tag.XML_ATTR_KEY),
                            cnode.getStringAttribute(Tag.XML_ATTR_VALUE)));
                }
            }

            config.putWorkFlowInfo(outputInfo.getId(), outputInfo);
        }
    }
}
