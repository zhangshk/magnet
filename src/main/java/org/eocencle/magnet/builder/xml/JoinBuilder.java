package org.eocencle.magnet.builder.xml;

import org.eocencle.magnet.builder.Tag;
import org.eocencle.magnet.mapping.FilterInfo;
import org.eocencle.magnet.mapping.JoinInfo;
import org.eocencle.magnet.parsing.XNode;
import org.eocencle.magnet.session.ProjectConfig;

import java.util.List;

/**
 * 连接建构类
 * @author: huan
 * @Date: 2020-04-04
 * @Description:
 */
public class JoinBuilder implements XMLParser {
    // 单例实体
    private static JoinBuilder BUILDER = new JoinBuilder();

    private JoinBuilder() {

    }

    /**
     * 获取单例实体
     * @Author huan
     * @Date 2020-04-04
     * @Param []
     * @Return org.eocencle.magnet.builder.xml.JoinBuilder
     * @Exception
     * @Description
     **/
    public static JoinBuilder getInstance() {
        return BUILDER;
    }

    @Override
    public void parse(XNode node, ProjectConfig config) {
        this.parseElements(node.evalNodes(Tag.XML_EL_JOIN), config);
    }

    /**
     * 解析元素
     * @Author huan
     * @Date 2020-04-04
     * @Param [nodes, config]
     * @Return void
     * @Exception
     * @Description
     **/
    private void parseElements(List<XNode> nodes, ProjectConfig config) {
        if (null == nodes) {
            return;
        }

        JoinInfo joinInfo = null;
        List<XNode> ons = null;
        String tagName = null;
        FilterInfo.FilterField onField = null;
        for (XNode node: nodes) {
            joinInfo = new JoinInfo();
            joinInfo.setId(node.getStringAttribute(Tag.XML_ATTR_ID));
            joinInfo.setAlias(node.getStringAttribute(Tag.XML_ATTR_ALIAS));
            joinInfo.setType(node.getStringAttribute(Tag.XML_ATTR_TYPE, Tag.JOIN_TYPE_INNER));
            joinInfo.setLeftRef(node.getStringAttribute(Tag.XML_ATTR_LEFTREF));
            joinInfo.setRightRef(node.getStringAttribute(Tag.XML_ATTR_RIGHTREF));
            ons = node.evalNode(Tag.XML_EL_ON).getChildren();
            for (XNode on: ons) {
                onField = new FilterInfo.FilterField();
                tagName = on.getName().toUpperCase();
                if (tagName.startsWith(Tag.FILTER_JOIN_AND)) {
                    onField.setJoin(Tag.FILTER_JOIN_AND);
                    onField.setType(tagName.substring(Tag.FILTER_JOIN_AND.length()));
                } else if (tagName.startsWith(Tag.FILTER_JOIN_OR)) {
                    onField.setJoin(Tag.FILTER_JOIN_OR);
                    onField.setType(tagName.substring(Tag.FILTER_JOIN_OR.length()));
                }
                onField.setField(on.getStringAttribute(Tag.XML_ATTR_FIELD));
                onField.setValue(on.getStringAttribute(Tag.XML_ATTR_VALUE));

                joinInfo.addFilterFields(onField);
            }

            config.putWorkFlowInfo(joinInfo.getId(), joinInfo);
        }
    }
}
