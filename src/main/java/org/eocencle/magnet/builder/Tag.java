package org.eocencle.magnet.builder;

/**
 * 标记类
 * @author: huan
 * @Date: 2020-01-18
 * @Description:
 */
public class Tag {
    // xml标签
    public static final String XML_EL_PROJECT		            = "project";
    public static final String XML_EL_DATASOURCE	            = "datasource";
    public static final String XML_EL_TABLE			            = "table";
    public static final String XML_EL_STREAM                    = "stream";
    public static final String XML_EL_CONFIG                    = "config";
    public static final String XML_EL_FIELDS		            = "fields";
    public static final String XML_EL_FIELD			            = "field";
    public static final String XML_EL_PARAMETER		            = "parameter";
    public static final String XML_EL_VARIABLE		            = "variable";
    public static final String XML_EL_WORKFLOW		            = "workflow";
    public static final String XML_EL_SQL			            = "sql";
    public static final String XML_EL_FILTER		            = "filter";
    public static final String XML_EL_CONDITIONS		        = "conditions";
    public static final String XML_EL_DISTINCT		            = "distinct";
    public static final String XML_EL_PROCESS		            = "process";
    public static final String XML_EL_UNION			            = "union";
    public static final String XML_EL_JOIN			            = "join";
    public static final String XML_EL_OUTPUT		            = "output";
    public static final String XML_EL_PROPERTY		            = "property";
    public static final String XML_EL_BRANCH		            = "branch";
    public static final String XML_EL_DATASETS		            = "datasets";
    public static final String XML_EL_DATASET		            = "dataset";
    public static final String XML_EL_ORDER			            = "order";
    public static final String XML_EL_ROWNUM		            = "rownum";
    public static final String XML_EL_GROUP			            = "group";
    public static final String XML_EL_TRIM			            = "trim";
    public static final String XML_EL_WHERE			            = "where";
    public static final String XML_EL_FOREACH		            = "foreach";
    public static final String XML_EL_IF			            = "if";
    public static final String XML_EL_CHOOSE		            = "choose";
    public static final String XML_EL_WHEN			            = "when";
    public static final String XML_EL_OTHERWISE		            = "otherwise";
    public static final String XML_EL_LIST		                = "list";
    public static final String XML_EL_VALUE		                = "value";
    public static final String XML_EL_MAP		                = "map";
    public static final String XML_EL_ENTRY		                = "entry";
    public static final String XML_EL_INCLUDE                   = "include";
    public static final String XML_EL_FRAGMENT                  = "fragment";
    public static final String XML_EL_SCRIPT                    = "script";
    public static final String XML_EL_ON                        = "on";
    public static final String XML_EL_SCHEMA                    = "schema";
    public static final String XML_EL_VALUEMAPPERS              = "valueMappers";

    // xml属性
    public static final String XML_ATTR_ID			    = "id";
    public static final String XML_ATTR_ALIAS		    = "alias";
    public static final String XML_ATTR_SRC			    = "src";
    public static final String XML_ATTR_NAME		    = "name";
    public static final String XML_ATTR_TYPE		    = "type";
    public static final String XML_ATTR_PRECISION	    = "precision";
    public static final String XML_ATTR_KEY			    = "key";
    public static final String XML_ATTR_VALUE		    = "value";
    public static final String XML_ATTR_FIELD		    = "field";
    public static final String XML_ATTR_START		    = "start";
    public static final String XML_ATTR_END			    = "end";
    public static final String XML_ATTR_REF			    = "ref";
    public static final String XML_ATTR_TARGET		    = "target";
    public static final String XML_ATTR_FORMAT		    = "format";
    public static final String XML_ATTR_ORDER		    = "order";
    public static final String XML_ATTR_ROWNUM		    = "rownum";
    public static final String XML_ATTR_SEPARATOR	    = "separator";
    public static final String XML_ATTR_CONTEXT	        = "context";
    public static final String XML_ATTR_VERSION	        = "version";
    public static final String XML_ATTR_PREFIX	        = "prefix";
    public static final String XML_ATTR_PREFIXOVERRIDES	= "prefixOverrides";
    public static final String XML_ATTR_SUFFIX	        = "suffix";
    public static final String XML_ATTR_SUFFIXOVERRIDES	= "suffixOverrides";
    public static final String XML_ATTR_COLLECTION	    = "collection";
    public static final String XML_ATTR_ITEM	        = "item";
    public static final String XML_ATTR_INDEX	        = "index";
    public static final String XML_ATTR_OPEN	        = "open";
    public static final String XML_ATTR_CLOSE	        = "close";
    public static final String XML_ATTR_TEST	        = "test";
    public static final String XML_ATTR_COMPRESS	    = "compress";
    public static final String XML_ATTR_TOPICS          = "topics";
    public static final String XML_ATTR_STYLE           = "style";
    public static final String XML_ATTR_REFS            = "refs";
    public static final String XML_ATTR_FIELDS          = "fields";
    public static final String XML_ATTR_LEFTREF			= "leftRef";
    public static final String XML_ATTR_RIGHTREF		= "rightRef";
    public static final String XML_ATTR_TAGFIELD		= "tagField";
    public static final String XML_ATTR_NONMATCH		= "nonMatch";
    public static final String XML_ATTR_SOURCE          = "source";

    // 应用默认名称
    public static final String CONTEXT_APPNAME_DEFAULT = "Magnet";

    // 上下文环境
    public static final String CONTEXT_SPARK = "Spark";
    public static final String CONTEXT_FLINK = "Flink";

    // 上下文环境默认版本
    public static final String CONTEXT_VERSION_DEFALUT = "1.6.0";

    // 文件格式
    public static final String FILE_FORMAT_TEXTFILE     = "TextFile";
    public static final String FILE_FORMAT_MAPFILE      = "MapFile";
    public static final String FILE_FORMAT_RCFILE       = "RCFile";
    public static final String FILE_FORMAT_ORCFILE      = "ORCFile";
    public static final String FILE_FORMAT_PARQUETFILE  = "ParquetFile";
    public static final String FILE_FORMAT_AVROFILE     = "AvroFile";
    public static final String FILE_FORMAT_JSONFILE     = "JsonFile";

    // 表字段类型
    public static final String TABLE_FIELD_TYPE_STRING              = "String";
    public static final String TABLE_FIELD_TYPE_BOOLEAN             = "Boolean";
    public static final String TABLE_FIELD_TYPE_BOOL                = "Bool";
    public static final String TABLE_FIELD_TYPE_DATETIME            = "DateTime";
    public static final String TABLE_FIELD_TYPE_DOUBLE              = "Double";
    public static final String TABLE_FIELD_TYPE_FLOAT               = "Float";
    public static final String TABLE_FIELD_TYPE_BYTE                = "Byte";
    public static final String TABLE_FIELD_TYPE_INTEGER             = "Integer";
    public static final String TABLE_FIELD_TYPE_INT                 = "Int";
    public static final String TABLE_FIELD_TYPE_LONG                = "Long";
    public static final String TABLE_FIELD_TYPE_SHORT               = "Short";
    public static final String TABLE_FIELD_TYPE_DECIMAL             = "Decimal";
    public static final String TABLE_FIELD_TYPE_BINARY              = "Binary";
    public static final String TABLE_FIELD_TYPE_TIMESTAMP           = "Timestamp";
    public static final String TABLE_FIELD_TYPE_CALENDARINTERVAL    = "CalendarInterval";

    // 输出方式
    public static final String OUTPUT_STYLE_FILE    = "file";
    public static final String OUTPUT_STYLE_BRANCH  = "branch";
    public static final String OUTPUT_STYLE_EMAIL   = "email";
    public static final String OUTPUT_STYLE_CONSOLE = "console";

    // 输出类型
    public static final String OUTPUT_TYPE_CREATE   = "create";
    public static final String OUTPUT_TYPE_OVERRIDE = "override";
    public static final String OUTPUT_TYPE_APPEND   = "append";

    // 邮件配置
    public static final String OUTPUT_EMAIL_HOST        = "host";
    public static final String OUTPUT_EMAIL_ACCOUT      = "accout";
    public static final String OUTPUT_EMAIL_PERSONAL    = "personal";
    public static final String OUTPUT_EMAIL_PWD         = "pwd";
    public static final String OUTPUT_EMAIL_USERS       = "users";
    public static final String OUTPUT_EMAIL_SUBJECT     = "subject";

    // 排序类型
    public static final String ORDERBY_ASC  = "asc";
    public static final String ORDERBY_DESC = "desc";

    // rownum默认字段名
    public static final String ROWNUM_DEFAULT = "rownum";

    // 数据源前缀
    public static final String SRC_PREFIX_HDFS 	= "hdfs://";
    public static final String SRC_PREFIX_FILE 	= "file:///";
    public static final String SRC_PREFIX_HTTP 	= "http://";
    public static final String SRC_PREFIX_JDBC 	= "jdbc:";

    // 字符
    public static final String STRING_BLANK     = "";
    public static final String STRING_SPACE     = " ";
    public static final String STRING_UNDERLINE = "_";
    public static final String STRING_COMMA     = ",";

    // 分割符
    public static final String SPLIT_COMMA          = ",";
    public static final String SPLIT_BLANK          = " ";
    public static final String SPLIT_INVISIBLE1     = "\001";

    // SQL
    public static final String SQL_WHERE    = "WHERE";
    public static final String SQL_SET      = "SET";

    // 压缩格式
    public static final String COMPRESS_NONE    = "None";
    public static final String COMPRESS_SNAPPY  = "Snappy";
    public static final String COMPRESS_DEFAULT = "Default";
    public static final String COMPRESS_GZIP    = "Gzip";
    public static final String COMPRESS_BZIP2   = "BZip2";
    public static final String COMPRESS_LZ4     = "Lz4";

    // 项目信息
    public static final String MAGNET_NAME          = "_AppName";
    public static final String MAGNET_CONTEXT       = "_Context";
    public static final String MAGNET_VERSION       = "_Version";
    public static final String MAGNET_FILE_PATH     = "_FilePath";
    public static final String MAGNET_FILE_NAME     = "_FileName";
    public static final String MAGNET_ENV_MODE      = "_EnvMode";
    public static final String MAGNET_PROCESS_MODE  = "_ProcessMode";
    public static final String MAGNET_DURATION      = "_Duration";
    public static final String MAGNET_SQL_ENGINE    = "_SQLEngine";

    // 环境模式
    public static final String ENV_MODE_LOCAL       = "Local";
    public static final String ENV_MODE_CLUSTER     = "Cluster";

    // 处理模式
    public static final String PROCESS_MODE_BATCH   = "Batch";
    public static final String PROCESS_MODE_STREAM  = "Stream";

    // 流式处理默认持续时间
    public static final String STREAM_DEFAULT_DURATION = "60";

    // SQL处理引擎
    public static final String SQL_ENGINE_SPARK = "SaprkSQL";
    public static final String SQL_ENGINE_HIVE  = "HiveSQL";

    // 过滤条件
    public static final String FILTER_JOIN_AND                          = "AND";
    public static final String FILTER_JOIN_OR                           = "OR";
    public static final String FILTER_CONDITION_ISNULL                  = "ISNULL";
    public static final String FILTER_CONDITION_ISNOTNULL               = "ISNOTNULL";
    public static final String FILTER_CONDITION_EQUALTO                 = "EQUALTO";
    public static final String FILTER_CONDITION_NOTEQUALTO              = "NOTEQUALTO";
    public static final String FILTER_CONDITION_GREATERTHAN             = "GREATERTHAN";
    public static final String FILTER_CONDITION_GREATERTHANOREQUALTO    = "GREATERTHANOREQUALTO";
    public static final String FILTER_CONDITION_LESSTHAN                = "LESSTHAN";
    public static final String FILTER_CONDITION_LESSTHANOREQUALTO       = "LESSTHANOREQUALTO";
    public static final String FILTER_CONDITION_IN                      = "IN";
    public static final String FILTER_CONDITION_BETWEEN                 = "BETWEEN";
    public static final String FILTER_CONDITION_PREFIX                  = "PREFIX";
    public static final String FILTER_CONDITION_SUFFIX                  = "SUFFIX";
    public static final String FILTER_CONDITION_CONTAIN                 = "CONTAIN";

    // 表数据源类型
    public static final String TABLE_STYLE_DEFAULT  = "default";
    public static final String TABLE_STYLE_FILE     = "file";
    public static final String TABLE_STYLE_HTTP     = "http";
    public static final String TABLE_STYLE_DATABASE = "database";
    public static final String TABLE_STYLE_KAFKA    = "kafka";

    // 数据库连接
    public static final String DB_TYPE      = "db.type";
    public static final String DB_HOST      = "db.host";
    public static final String DB_PORT      = "db.port";
    public static final String DB_DATABASE  = "db.database";
    public static final String DB_USERNAME  = "db.username";
    public static final String DB_PASSWORD  = "db.password";

    // 数据库类型
    public static final String DB_TYPE_MYSQL        = "mysql";
    public static final String DB_TYPE_SQLSERVER    = "sqlserver";
    public static final String DB_TYPE_ORACLE       = "oracle";

    // 数据库默认端口
    public static final String DB_PORT_MYSQL       = "3306";
    public static final String DB_PORT_SQLSERVER   = "1433";
    public static final String DB_PORT_ORACLE      = "1521";

    // 连接类型
    public static final String JOIN_TYPE_INNER          = "inner";
    public static final String JOIN_TYPE_OUTER          = "outer";
    public static final String JOIN_TYPE_LEFTOUTER      = "left_outer";
    public static final String JOIN_TYPE_RIGHTOUTER     = "right_outer";
    public static final String JOIN_TYPE_LEFTSEMI       = "leftsemi";
}
