package org.eocencle.magnet.mapping;

/**
 * SQL脚本类
 * @author: huan
 * @Date: 2020-01-18
 * @Description:
 */
public class SQLScriptInfo extends WorkFlowInfo {
    // 别名
    private String alias;
    // sql源
    private SQLSource sqlSource;
    // sql
    private String sql;

    public SQLScriptInfo(SQLSource sqlSource) {
        this.sqlSource = sqlSource;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public SQLSource getSqlSource() {
        return sqlSource;
    }

    public void setSqlSource(SQLSource sqlSource) {
        this.sqlSource = sqlSource;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    @Override
    public String toString() {
        return this.sql;
    }
}
