package org.eocencle.magnet.mapping;

/**
 * 去重信息类
 * @author: huan
 * @Date: 2020-03-15
 * @Description:
 */
public class DistinctInfo extends WorkFlowInfo {
    // 别名
    private String alias;
    // 引用
    private String ref;

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }
}
