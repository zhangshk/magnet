package org.eocencle.magnet.component;

import org.eocencle.magnet.mapping.SQLScriptInfo;

/**
 * SQL作业节点抽象类
 * @author: huan
 * @Date: 2020-01-12
 * @Description:
 */
public abstract class SQLWorkStage extends WorkStageComponent {
    // SQL脚本信息
    protected SQLScriptInfo sqlScriptInfo;
}
