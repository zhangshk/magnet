package org.eocencle.magnet.component.wrapper;

import org.eocencle.magnet.component.PrepareDataWrapper;
import org.eocencle.magnet.component.PrepareOperateWrapper;
import org.eocencle.magnet.component.WorkStageComponent;
import org.eocencle.magnet.component.WorkStageOperate;
import org.eocencle.magnet.mapping.SQLScriptInfo;
import org.eocencle.magnet.mapping.WorkStageInfo;
import org.eocencle.magnet.util.StrictMap;

/**
 * 包装管理类
 * @author: huan
 * @Date: 2020-02-24
 * @Description:
 */
public class WrapperManager {
    // 数据准备包装map
    private static StrictMap<PrepareDataWrapper> prepareDataWrapperMap =
            new StrictMap<PrepareDataWrapper>("Prepare data wrapper map");
    // 操作准备包装map
    private static StrictMap<PrepareOperateWrapper> prepareOperateWrapperMap =
            new StrictMap<PrepareOperateWrapper>("Prepare operate wrapper map");
    // 执行包装map
    private static StrictMap<WorkStageComponentWrapper> workStageComponentWrapperMap =
            new StrictMap<WorkStageComponentWrapper>("Running component wrapper map");

    static {

    }

    /**
     * 组件包装
     * @Author huan
     * @Date 2020-02-27
     * @Param [component, info, operate]
     * @Return org.eocencle.magnet.component.WorkStageComponent
     * @Exception
     * @Description
     **/
    public static WorkStageComponent wrapper(WorkStageComponent component, WorkStageInfo info, WorkStageOperate operate) {
        component.initData(info);
        component.initOperate(operate);

        // SQL脚本
        if (info instanceof SQLScriptInfo) {
            component = new SQLScriptParserWrapper(component);
            component.initData(info);
            component.initOperate(operate);
        }

        // 参数替换
        component = new ParameterReplaceWrapper(component);
        component.initData(info);
        component.initOperate(operate);

        // 收集结果
        component = new CollectResultWrapper(component);
        component.initData(info);
        component.initOperate(operate);

        // 注册SparkSQL表
        component = new SparkSQLTableRegisterWrapper(component);
        component.initData(info);
        component.initOperate(operate);
        return component;
    }

}
