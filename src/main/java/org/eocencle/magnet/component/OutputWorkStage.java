package org.eocencle.magnet.component;

import org.eocencle.magnet.mapping.OutputInfo;

/**
 * 输出作业节点抽象类
 * @author: huan
 * @Date: 2020-01-12
 * @Description:
 */
public abstract class OutputWorkStage extends WorkStageComponent {
    // 输出信息
    protected OutputInfo outputInfo;
}
