package org.eocencle.magnet.component;

import org.eocencle.magnet.mapping.TableInfo;

/**
 * 表作业节点抽象类
 * @author: huan
 * @Date: 2020-01-12
 * @Description:
 */
public abstract class TableWorkStage extends WorkStageComponent {
    // 表信息
    protected TableInfo tableInfo;
}
