package org.eocencle.magnet.component;

import org.eocencle.magnet.context.factory.RuntimeEnvironmentFactory;

/**
 * 作业节点建构辅助类
 * @author: huan
 * @Date: 2020-01-21
 * @Description:
 */
public class WorkStageComponentBuilderAssistant {
    // 执行环境工厂
    private static RuntimeEnvironmentFactory FACTORY;

    public static RuntimeEnvironmentFactory getFactory() {
        return FACTORY;
    }

    public static void setFactory(RuntimeEnvironmentFactory factory) {
        FACTORY = factory;
    }
}
