package org.eocencle.magnet.component;

import org.eocencle.magnet.builder.Tag;
import org.eocencle.magnet.component.wrapper.WrapperManager;
import org.eocencle.magnet.context.factory.RuntimeEnvironmentFactory;
import org.eocencle.magnet.exception.UnsupportedException;
import org.eocencle.magnet.mapping.*;
import org.eocencle.magnet.session.ProjectConfig;
import org.eocencle.magnet.util.StrictMap;

import java.util.Map;

/**
 * 作业节点建构类
 * @author: huan
 * @Date: 2020-01-12
 * @Description:
 */
public class WorkStageComponentBuilder {
    /**
     * 构造作业节点组件
     * @Author huan
     * @Date 2020-01-21
     * @Param [config]
     * @Return org.eocencle.magnet.component.WorkStageComponent
     * @Exception
     * @Description
     **/
    public static WorkStageComponent construct(ProjectConfig config) {
        BranchInfo info = new BranchInfo();
        info.setId("main");
        info.setProjectConfig(config);
        return construct(config, info);
    }

    /**
     * 构造作业节点组件
     * @Author huan
     * @Date 2020-02-06
     * @Param [config, branch]
     * @Return org.eocencle.magnet.component.WorkStageComponent
     * @Exception
     * @Description
     **/
    public static WorkStageComponent construct(ProjectConfig config, BranchInfo branch) {
        // 执行容器
        WorkStageComposite mainComposite = new WorkStageComposite();
        // 初始化执行容器
        mainComposite.initData(branch);
        // 执行环境组件工厂
        RuntimeEnvironmentFactory factory = WorkStageComponentBuilderAssistant.getFactory();
        // 数据源
        DataSourceInfo dataSourceInfo = null;
        // 组件
        WorkStageComponent component = null;
        // 操作
        WorkStageOperate operate = null;
        // 流组件
        WorkStageComponent streamComponent = null;

        // 构造参数
        mainComposite.setParams((StrictMap<Object>) config.getParameterInfo().clone());

        // 构造数据源组件
        StrictMap<DataSourceInfo> datasource = config.getDataSourceInfo();
        for (Map.Entry<String, DataSourceInfo> entry: datasource.entrySet()) {
            dataSourceInfo = entry.getValue();
            if (dataSourceInfo instanceof TableInfo) {
                // 表数据
                component = factory.createTableWorkStageComponent();
                operate = factory.createTableWorkStageOperate((TableInfo) dataSourceInfo);
            } else if (dataSourceInfo instanceof StreamInfo) {
                // 流数据
                streamComponent = factory.createStreamWorkStageComponent();
                streamComponent.initData(dataSourceInfo);
                streamComponent.initOperate(factory.createStreamWorkStageOperate((StreamInfo) dataSourceInfo));
                continue;
            }

            mainComposite.add(WrapperManager.wrapper(component, dataSourceInfo, operate));
        }

        // 构造工作流组件
        StrictMap<WorkFlowInfo> workflow = config.getWorkFlowInfo();
        if (Tag.PROCESS_MODE_BATCH.equalsIgnoreCase(config.getParameterInfo(Tag.MAGNET_PROCESS_MODE).toString())) {
            buildWorkflow(mainComposite, workflow);
        } else if (Tag.PROCESS_MODE_STREAM.equalsIgnoreCase(config.getParameterInfo(Tag.MAGNET_PROCESS_MODE).toString())) {
            mainComposite.add(streamComponent);
            buildWorkflow(streamComponent, workflow);
        }

        return mainComposite;
    }

    /**
     * 建构工作流
     * @Author huan
     * @Date 2020-03-18
     * @Param [container, workflow]
     * @Return void
     * @Exception
     * @Description
     **/
    private static void buildWorkflow(WorkStageComponent container, StrictMap<WorkFlowInfo> workflow) {
        // 执行环境组件工厂
        RuntimeEnvironmentFactory factory = WorkStageComponentBuilderAssistant.getFactory();
        // 工作流信息
        WorkFlowInfo workFlowInfo = null;
        // 组件
        WorkStageComponent component = null;
        // 操作
        WorkStageOperate operate = null;
        // 分支配置
        ProjectConfig branchProjectConfig = null;

        for (Map.Entry<String, WorkFlowInfo> entry: workflow.entrySet()) {
            workFlowInfo = entry.getValue();
            if (workFlowInfo instanceof BranchInfo) {
                // 分支
                BranchInfo branchInfo = (BranchInfo) workFlowInfo;
                branchProjectConfig = branchInfo.getProjectConfig();
                branchProjectConfig.putParameterInfo(Tag.MAGNET_FILE_NAME, branchInfo.getFileName());
                branchProjectConfig.putParameterInfo(Tag.MAGNET_FILE_PATH, branchInfo.getSrc());
                component = construct(branchProjectConfig, branchInfo);
                operate = null;
            } else if (workFlowInfo instanceof SQLScriptInfo) {
                // SQL解析
                component = factory.createSQLWorkStageComponent();
                operate = factory.createSQLWorkStageOperate((SQLScriptInfo) workFlowInfo);
            } else if (workFlowInfo instanceof OutputInfo) {
                // 输出
                component = factory.createOutputWorkStageComponent();
                operate = factory.createOutputWorkStageOperate((OutputInfo) workFlowInfo);
            } else if (workFlowInfo instanceof GroupInfo) {
                // 分组
                component = factory.createGroupWorkStageComponent();
                operate = factory.createGroupWorkStageOperate((GroupInfo) workFlowInfo);
            } else if (workFlowInfo instanceof FilterInfo) {
                // 过滤
                component = factory.createFilterWorkStageComponent();
                operate = factory.createFilterWorkStageOperate((FilterInfo) workFlowInfo);
            } else if (workFlowInfo instanceof DistinctInfo) {
                // 去重
                component = factory.createDistinctWorkStageComponent();
                operate = null;
            } else if (workFlowInfo instanceof OrderInfo) {
                // 排序
                component = factory.createOrderWorkStageComponent();
                operate = factory.createOrderWorkStageOperate((OrderInfo) workFlowInfo);
            } else if (workFlowInfo instanceof UnionInfo) {
                // 合并
                component = factory.createUnionWorkStageComponent();
                operate = null;
            } else if (workFlowInfo instanceof JoinInfo) {
                // 关联
                component = factory.createJoinWorkStageComponent();
                operate = factory.createJoinWorkStageOperate((JoinInfo) workFlowInfo);
            } else if (workFlowInfo instanceof SchemaInfo) {
                // Schema
                component = factory.createSchemaWorkStageComponent();
                operate = null;
            } else {
                throw new UnsupportedException("Unsupported workflow type " + workFlowInfo.getClass().getName() + "!");
            }

            container.add(WrapperManager.wrapper(component, workFlowInfo, operate));
        }
    }
}
