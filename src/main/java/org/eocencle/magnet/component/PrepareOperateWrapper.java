package org.eocencle.magnet.component;

/**
 * 准备时操作包装接口
 * @author: huan
 * @Date: 2020-02-23
 * @Description:
 */
public interface PrepareOperateWrapper {
    /**
     * 初始化操作
     * @Author huan
     * @Date 2020-02-23
     * @Param [operate]
     * @Return void
     * @Exception
     * @Description
     **/
    void initOperate(WorkStageOperate operate);

}
