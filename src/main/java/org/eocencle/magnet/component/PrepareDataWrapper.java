package org.eocencle.magnet.component;

import org.eocencle.magnet.mapping.WorkStageInfo;

/**
 * 准备时数据包装接口
 * @author: huan
 * @Date: 2020-02-23
 * @Description:
 */
public interface PrepareDataWrapper {
    /**
     * 初始化数据
     * @Author huan
     * @Date 2020-02-23
     * @Param [info]
     * @Return void
     * @Exception
     * @Description
     **/
    void initData(WorkStageInfo info);

}
