package org.eocencle.magnet.component;

import java.io.Serializable;

/**
 * 作业节点操作接口
 * @author: huan
 * @Date: 2020-02-23
 * @Description:
 */
public interface WorkStageOperate extends Serializable {
}
