package org.eocencle.magnet.component;

import org.eocencle.magnet.builder.Tag;
import org.eocencle.magnet.component.wrapper.RunningContainerWrapper;
import org.eocencle.magnet.context.Context;
import org.eocencle.magnet.context.RuntimeEnvironmentBuilder;
import org.eocencle.magnet.context.factory.RuntimeEnvironmentFactory;
import org.eocencle.magnet.session.ProjectConfig;
import org.eocencle.magnet.session.RuntimeEnvironmentConfig;

/**
 * 工作组件外观类
 * @author: huan
 * @Date: 2020-02-09
 * @Description:
 */
public class WorkStageComponentFacade {
    // 项目配置
    private ProjectConfig config;

    public WorkStageComponentFacade(ProjectConfig config) {
        this.config = config;
    }

    /**
     * 构建并执行组件
     * @Author huan
     * @Date 2020-02-09
     * @Param []
     * @Return void
     * @Exception
     * @Description
     **/
    public void execute() {
        RuntimeEnvironmentConfig reConfig = new RuntimeEnvironmentConfig(this.config);
        // 构建环境
        RuntimeEnvironmentFactory factory = RuntimeEnvironmentBuilder.construct(reConfig);
        // 设置构建助理
        WorkStageComponentBuilderAssistant.setFactory(factory);
        // 构建主要执行组件
        WorkStageComponent composite = WorkStageComponentBuilder.construct(this.config);
        // 包装执行环境
        WorkStageComponent component = new RunningContainerWrapper(composite);
        // 获取执行上下文
        Context context = factory.getRuntimeContext();
        // 设置应用名称
        context.setAppName(this.config.getParameterInfo(Tag.MAGNET_NAME).toString());
        // 设置应用环境模式
        context.setEnvMode(this.config.getParameterInfo(Tag.MAGNET_ENV_MODE).toString());
        // 设置应用处理模式
        context.setProcessMode(this.config.getParameterInfo(Tag.MAGNET_PROCESS_MODE).toString());
        // 设置流式处理的持续时间
        context.setDuration(Integer.parseInt(this.config.getParameterInfo(Tag.MAGNET_DURATION).toString()));
        // 设置SQL引擎
        context.setSqlEngine(this.config.getParameterInfo(Tag.MAGNET_SQL_ENGINE).toString());
        // 构建工作流参数
        WorkStageParameter parameter = new WorkStageParameter(context);
        // 执行组件
        component.execute(parameter);
    }

}
