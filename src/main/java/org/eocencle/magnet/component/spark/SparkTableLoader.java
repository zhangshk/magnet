package org.eocencle.magnet.component.spark;

import org.eocencle.magnet.component.WorkStageOperate;
import org.eocencle.magnet.mapping.TableInfo;

/**
 * Spark表加载器抽象类
 * @author: huan
 * @Date: 2020-02-01
 * @Description:
 */
public abstract class SparkTableLoader implements WorkStageOperate {
    // 表信息
    protected TableInfo tableInfo;

    public SparkTableLoader(TableInfo tableInfo) {
        this.tableInfo = tableInfo;
    }
}
