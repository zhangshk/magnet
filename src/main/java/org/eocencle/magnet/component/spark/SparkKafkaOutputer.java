package org.eocencle.magnet.component.spark;

import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.spark.sql.Row;
import org.eocencle.magnet.builder.Tag;
import org.eocencle.magnet.component.WorkStageResult;
import org.eocencle.magnet.mapping.InfoParam;
import org.eocencle.magnet.mapping.OutputInfo;

import java.util.Map;
import java.util.Properties;

/**
 * Spark的kafka输出器类
 * @author: huan
 * @Date: 2020-04-02
 * @Description:
 */
public class SparkKafkaOutputer implements SparkOutputer {
    @Override
    public WorkStageResult output(OutputInfo outputInfo, SparkWorkStageResult result) {
        Properties properties = new Properties();
        for (Map.Entry<String, InfoParam> entry: outputInfo.getOutputConfig().entrySet()) {
            properties.put(entry.getKey(), entry.getValue().getValue());
        }
        KafkaProducer<String, String> producer = new KafkaProducer<>(properties);
        result.getRdd().foreach((Row row) -> {
            String[] val = new String[row.length()];
            for (int i = 0; i < row.length(); i++) {
                if (row.isNullAt(i)) {
                    val[i] = Tag.STRING_BLANK;
                } else {
                    val[i] = row.get(i).toString();
                }
            }
            producer.send(new ProducerRecord<>(outputInfo.getTarget(), StringUtils.join(val, outputInfo.getSeparator())));
        });
        producer.close();
        return null;
    }
}
