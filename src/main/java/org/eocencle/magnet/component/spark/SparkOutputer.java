package org.eocencle.magnet.component.spark;

import org.eocencle.magnet.component.WorkStageOperate;
import org.eocencle.magnet.component.WorkStageResult;
import org.eocencle.magnet.mapping.OutputInfo;

/**
 * Spark输出器接口
 * @author: huan
 * @Date: 2020-02-01
 * @Description:
 */
public interface SparkOutputer extends WorkStageOperate {
    /**
     * 输出
     * @Author huan
     * @Date 2020-02-01
     * @Param [outputInfo, result]
     * @Return org.eocencle.magnet.component.WorkStageResult
     * @Exception
     * @Description
     **/
    WorkStageResult output(OutputInfo outputInfo, SparkWorkStageResult result);
}
