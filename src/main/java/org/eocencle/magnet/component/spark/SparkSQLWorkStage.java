package org.eocencle.magnet.component.spark;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.eocencle.magnet.component.*;
import org.eocencle.magnet.context.Context;
import org.eocencle.magnet.context.factory.RuntimeEnvironmentFactory;
import org.eocencle.magnet.mapping.SQLScriptInfo;
import org.eocencle.magnet.mapping.WorkStageInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * sparkSQL作业节点类
 * @author: huan
 * @Date: 2020-01-22
 * @Description:
 */
public class SparkSQLWorkStage extends SQLWorkStage {

    @Override
    public void initData(WorkStageInfo info) {
        this.sqlScriptInfo = (SQLScriptInfo) info;
    }

    @Override
    public void initOperate(WorkStageOperate operate) {

    }

    @Override
    public List<WorkStageResult> execute(WorkStageParameter parameter) {
        RuntimeEnvironmentFactory factory = WorkStageComponentBuilderAssistant.getFactory();
        Context context = parameter.getContext();

        // 创建DataFrame
        DataFrame df = this.createDataFrame((SQLContext) context.getSQLContext(), this.sqlScriptInfo.toString());
        // 创建RDD
        JavaRDD<Row> rdd = this.createRDD(df);

        // 设置返回值
        SparkWorkStageResult result = (SparkWorkStageResult) factory.createWorkStageResult();
        result.setId(this.sqlScriptInfo.getId());
        result.setAlias(this.sqlScriptInfo.getAlias());
        result.setRdd(rdd);
        result.setDf(df);
        List<WorkStageResult> list = new ArrayList<>();
        list.add(result);
        return list;
    }

    private JavaRDD<Row> createRDD(DataFrame df) {
        return df.toJavaRDD();
    }

    private DataFrame createDataFrame(SQLContext context, String sql) {
        return context.sql(sql);
    }
}
