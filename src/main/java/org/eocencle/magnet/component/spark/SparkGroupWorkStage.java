package org.eocencle.magnet.component.spark;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.eocencle.magnet.component.*;
import org.eocencle.magnet.context.Context;
import org.eocencle.magnet.context.factory.RuntimeEnvironmentFactory;
import org.eocencle.magnet.mapping.GroupInfo;
import org.eocencle.magnet.mapping.WorkStageInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * spark组作业节点类
 * @author: huan
 * @Date: 2020-02-02
 * @Description:
 */
public class SparkGroupWorkStage extends GroupWorkStage {
    // 分组器
    private SparkGrouper grouper;

    @Override
    public void initData(WorkStageInfo info) {
        this.groupInfo = (GroupInfo) info;
    }

    @Override
    public void initOperate(WorkStageOperate operate) {
        this.grouper = (SparkGrouper) operate;
    }

    @Override
    public List<WorkStageResult> execute(WorkStageParameter parameter) {
        RuntimeEnvironmentFactory factory = WorkStageComponentBuilderAssistant.getFactory();
        Context context = parameter.getContext();

        SparkWorkStageResult prevResult =
                (SparkWorkStageResult) this.getParent().getPrevResult(this.groupInfo.getRef());

        // 创建RDD
        JavaRDD<Row> rdd = this.grouper.createRDD(this.groupInfo, prevResult);
        // 创建DataFrame
        DataFrame df = this.grouper.createDataFrame((SQLContext) context.getSQLContext(), this.groupInfo, prevResult, rdd);

        // 设置返回值
        SparkWorkStageResult result = (SparkWorkStageResult) factory.createWorkStageResult();
        result.setId(this.groupInfo.getId());
        result.setAlias(this.groupInfo.getAlias());
        result.setRdd(rdd);
        result.setDf(df);
        List<WorkStageResult> list = new ArrayList<>();
        list.add(result);
        return list;
    }

}
