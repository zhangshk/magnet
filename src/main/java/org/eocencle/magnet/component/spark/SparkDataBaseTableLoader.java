package org.eocencle.magnet.component.spark;

import org.apache.commons.lang3.StringUtils;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.eocencle.magnet.builder.Tag;
import org.eocencle.magnet.context.Context;
import org.eocencle.magnet.exception.UnsupportedException;
import org.eocencle.magnet.mapping.TableInfo;

import java.util.Properties;

/**
 * Spark数据库表作业节点类
 * @author: huan
 * @Date: 2020-03-26
 * @Description:
 */
public class SparkDataBaseTableLoader extends SparkTableDataFrameLoader {

    public SparkDataBaseTableLoader(TableInfo tableInfo) {
        super(tableInfo);
    }

    /**
     * 创建RDD
     * @Author huan
     * @Date 2020-03-26
     * @Param [df]
     * @Return org.apache.spark.api.java.JavaRDD<org.apache.spark.sql.Row>
     * @Exception
     * @Description
     **/
    @Override
    public JavaRDD<Row> createRDD(DataFrame df) {
        return df.toJavaRDD();
    }

    /**
     * 创建DataFrame
     * @Author huan
     * @Date 2020-03-26
     * @Param [context, src]
     * @Return org.apache.spark.sql.DataFrame
     * @Exception
     * @Description
     **/
    @Override
    public DataFrame createDataFrame(Context context, String src) {
        Properties properties = new Properties();
        properties.put("user", this.tableInfo.getConfigParam(Tag.DB_USERNAME));
        properties.put("password", this.tableInfo.getConfigParam(Tag.DB_PASSWORD));
        return ((SQLContext) context.getSQLContext()).read().jdbc(this.getDBUrl(), src, properties);
    }

    /**
     * 获取数据库URL
     * @Author huan
     * @Date 2020-03-26
     * @Param []
     * @Return java.lang.String
     * @Exception
     * @Description
     **/
    private String getDBUrl() {
        String type = this.tableInfo.getConfigParam(Tag.DB_TYPE);
        String host = this.tableInfo.getConfigParam(Tag.DB_HOST);
        String port = this.tableInfo.getConfigParam(Tag.DB_PORT);
        String dbName = this.tableInfo.getConfigParam(Tag.DB_DATABASE);
        if (Tag.DB_TYPE_MYSQL.equalsIgnoreCase(type)) {
            if (StringUtils.isBlank(port)) {
                port = Tag.DB_PORT_MYSQL;
            }
            return "jdbc:mysql://" + host + ":" + port + "/" + dbName;
        } else if (Tag.DB_TYPE_SQLSERVER.equalsIgnoreCase(type)) {
            if (StringUtils.isBlank(port)) {
                port = Tag.DB_PORT_SQLSERVER;
            }
            return "jdbc:sqlserver://" + host + ":" + port + ";DatabaseName=" + dbName;
        } else if (Tag.DB_TYPE_ORACLE.equalsIgnoreCase(type)) {
            if (StringUtils.isBlank(port)) {
                port = Tag.DB_PORT_ORACLE;
            }
            return "jdbc:oracle:thin:@" + host + ":" + port + ":" + dbName;
        }
        throw new UnsupportedException(type + " database type is not supported!");
    }
}
