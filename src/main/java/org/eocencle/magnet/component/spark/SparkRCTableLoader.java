package org.eocencle.magnet.component.spark;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.eocencle.magnet.context.Context;
import org.eocencle.magnet.mapping.TableInfo;

/**
 * SparkRC格式表作业节点类
 * @author: huan
 * @Date: 2020-01-12
 * @Description:
 */
public class SparkRCTableLoader extends SparkTableDataFrameLoader {

    public SparkRCTableLoader(TableInfo tableInfo) {
        super(tableInfo);
    }

    @Override
    public JavaRDD<Row> createRDD(DataFrame df) {
        return null;
    }

    @Override
    public DataFrame createDataFrame(Context context, String src) {
        return null;
    }

}
