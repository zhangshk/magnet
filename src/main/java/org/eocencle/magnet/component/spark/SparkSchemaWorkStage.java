package org.eocencle.magnet.component.spark;

import org.apache.log4j.Logger;
import org.eocencle.magnet.component.SchemaWorkStage;
import org.eocencle.magnet.component.WorkStageOperate;
import org.eocencle.magnet.component.WorkStageParameter;
import org.eocencle.magnet.component.WorkStageResult;

import java.util.List;

/**
 * Spark的Schema作业节点类
 * @author: huan
 * @Date: 2020-04-06
 * @Description:
 */
public class SparkSchemaWorkStage extends SchemaWorkStage {
    private static final Logger logger = Logger.getLogger(SparkSchemaWorkStage.class);

    @Override
    public void initOperate(WorkStageOperate operate) {

    }

    @Override
    public List<WorkStageResult> execute(WorkStageParameter parameter) {
        // 获取引用结果
        SparkWorkStageResult ref =
                (SparkWorkStageResult) this.getParent().getPrevResult(this.schemaInfo.getRef());

        // 输出schema
        logger.info(ref.getId() + "'s schema:");
        ref.getDf().printSchema();

        return null;
    }
}
