package org.eocencle.magnet.component.spark;

import org.apache.commons.lang3.StringUtils;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.types.*;
import org.eocencle.magnet.builder.Tag;
import org.eocencle.magnet.mapping.DataSourceField;
import org.eocencle.magnet.mapping.TableInfo;
import org.eocencle.magnet.util.StrictMap;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Spark文本表加载类
 * @author: huan
 * @Date: 2020-01-12
 * @Description:
 */
public class SparkTextTableLoader extends SparkTableRDDLoader {

    public SparkTextTableLoader(TableInfo tableInfo) {
        super(tableInfo);
    }

    /**
     * 创建RDD
     * @Author huan
     * @Date 2020-01-21
     * @Param [context, src]
     * @Return org.apache.spark.api.java.JavaRDD<org.apache.spark.sql.Row>
     * @Exception
     * @Description
     **/
    @Override
    public JavaRDD<Row> createRDD(JavaSparkContext context, String src) {
        JavaRDD<String> lines = context.textFile(src);

        return lines.map((String line) -> {
            String[] row = line.split(tableInfo.getSeparator(), -1);
            StrictMap<DataSourceField> dst = tableInfo.getFields();
            Object[] fields = new Object[dst.size()];
            int i = 0;
            String type = null;
            SimpleDateFormat tagSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            for (Map.Entry<String, DataSourceField> entry: dst.entrySet()) {
                type = entry.getValue().getType();

                if (Tag.TABLE_FIELD_TYPE_STRING.equalsIgnoreCase(type)) {
                    fields[i] = row[i];
                } else if (Tag.TABLE_FIELD_TYPE_BOOLEAN.equalsIgnoreCase(type)
                        || Tag.TABLE_FIELD_TYPE_BOOL.equalsIgnoreCase(type)) {
                    if (StringUtils.isNotBlank(row[i])) {
                        fields[i] = Boolean.valueOf(row[i]);
                    } else {
                        fields[i] = null;
                    }
                } else if (Tag.TABLE_FIELD_TYPE_DATETIME.equalsIgnoreCase(type)) {
                    if (StringUtils.isNotBlank(row[i])) {
                        SimpleDateFormat srcSdf = new SimpleDateFormat(entry.getValue().getFormat());
                        fields[i] = Timestamp.valueOf(tagSdf.format(srcSdf.parse(row[i])));
                    } else {
                        fields[i] = null;
                    }
                } else if (Tag.TABLE_FIELD_TYPE_DOUBLE.equalsIgnoreCase(type)) {
                    if (StringUtils.isNotBlank(row[i])) {
                        fields[i] = Double.valueOf(row[i]);
                    } else {
                        fields[i] = null;
                    }
                } else if (Tag.TABLE_FIELD_TYPE_FLOAT.equalsIgnoreCase(type)) {
                    if (StringUtils.isNotBlank(row[i])) {
                        fields[i] = Float.valueOf(row[i]);
                    } else {
                        fields[i] = null;
                    }
                } else if (Tag.TABLE_FIELD_TYPE_BYTE.equalsIgnoreCase(type)) {
                    if (StringUtils.isNotBlank(row[i])) {
                        fields[i] = Byte.valueOf(row[i]);
                    } else {
                        fields[i] = null;
                    }
                } else if (Tag.TABLE_FIELD_TYPE_INTEGER.equalsIgnoreCase(type)
                        || Tag.TABLE_FIELD_TYPE_INT.equalsIgnoreCase(type)) {
                    if (StringUtils.isNotBlank(row[i])) {
                        fields[i] = Integer.valueOf(row[i]);
                    } else {
                        fields[i] = null;
                    }
                } else if (Tag.TABLE_FIELD_TYPE_LONG.equalsIgnoreCase(type)) {
                    if (StringUtils.isNotBlank(row[i])) {
                        fields[i] = Long.valueOf(row[i]);
                    } else {
                        fields[i] = null;
                    }
                } else if (Tag.TABLE_FIELD_TYPE_SHORT.equalsIgnoreCase(type)) {
                    if (StringUtils.isNotBlank(row[i])) {
                        fields[i] = Short.valueOf(row[i]);
                    } else {
                        fields[i] = null;
                    }
                } else if (Tag.TABLE_FIELD_TYPE_DECIMAL.equalsIgnoreCase(type)) {
                    if (StringUtils.isNotBlank(row[i])) {
                        fields[i] = Decimal.apply(row[i]);
                    } else {
                        fields[i] = null;
                    }
                } else {
                    fields[i] = row[i];
                }

                i ++;
            }
            return RowFactory.create(fields);
        });
    }

    /**
     * 创建DataFrame
     * @Author huan
     * @Date 2020-01-21
     * @Param [sc, dst, rdd]
     * @Return org.apache.spark.sql.DataFrame
     * @Exception
     * @Description
     **/
    @Override
    public DataFrame createDataFrame(SQLContext sqlContext, StrictMap<DataSourceField> dst, JavaRDD<Row> rdd) {
        List<StructField> structFields = new ArrayList<StructField>();
        String name = null, type = null;
        DataType fieldType = null;
        DataSourceField field = null;
        for (Map.Entry<String, DataSourceField> entry: dst.entrySet()) {
            field = entry.getValue();
            name = field.getName();
            type = field.getType();

            if (Tag.TABLE_FIELD_TYPE_STRING.equalsIgnoreCase(type)) {
                fieldType = DataTypes.StringType;
            } else if (Tag.TABLE_FIELD_TYPE_BINARY.equalsIgnoreCase(type)) {
                fieldType = DataTypes.BinaryType;
            } else if (Tag.TABLE_FIELD_TYPE_BOOLEAN.equalsIgnoreCase(type)
                    || Tag.TABLE_FIELD_TYPE_BOOL.equalsIgnoreCase(type)) {
                fieldType = DataTypes.BooleanType;
            } else if (Tag.TABLE_FIELD_TYPE_DATETIME.equalsIgnoreCase(type) ||
                    Tag.TABLE_FIELD_TYPE_TIMESTAMP.equalsIgnoreCase(type)) {
                fieldType = DataTypes.TimestampType;
            } else if (Tag.TABLE_FIELD_TYPE_CALENDARINTERVAL.equalsIgnoreCase(type)) {
                fieldType = DataTypes.CalendarIntervalType;
            } else if (Tag.TABLE_FIELD_TYPE_DOUBLE.equalsIgnoreCase(type)) {
                fieldType = DataTypes.DoubleType;
            } else if (Tag.TABLE_FIELD_TYPE_FLOAT.equalsIgnoreCase(type)) {
                fieldType = DataTypes.FloatType;
            } else if (Tag.TABLE_FIELD_TYPE_BYTE.equalsIgnoreCase(type)) {
                fieldType = DataTypes.ByteType;
            } else if (Tag.TABLE_FIELD_TYPE_INTEGER.equalsIgnoreCase(type)
                    || Tag.TABLE_FIELD_TYPE_INT.equalsIgnoreCase(type)) {
                fieldType = DataTypes.IntegerType;
            } else if (Tag.TABLE_FIELD_TYPE_LONG.equalsIgnoreCase(type)) {
                fieldType = DataTypes.LongType;
            } else if (Tag.TABLE_FIELD_TYPE_SHORT.equalsIgnoreCase(type)) {
                fieldType = DataTypes.ShortType;
            } else if (Tag.TABLE_FIELD_TYPE_DECIMAL.equalsIgnoreCase(type)) {
                if (StringUtils.isNotBlank(field.getPrecision())) {
                    String[] precision = field.getPrecision().split(Tag.SPLIT_COMMA);
                    if (2 == precision.length) {
                        fieldType = DataTypes.createDecimalType(Integer.parseInt(precision[0]),Integer.parseInt(precision[1]));
                    } else {
                        fieldType = DataTypes.createDecimalType();
                    }
                } else {
                    fieldType = DataTypes.createDecimalType();
                }
            } else {
                fieldType = DataTypes.NullType;
            }

            structFields.add(DataTypes.createStructField(name, fieldType, true));
        }
        StructType structType = DataTypes.createStructType(structFields);
        return sqlContext.createDataFrame(rdd, structType);
    }
}
