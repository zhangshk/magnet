package org.eocencle.magnet.component.spark;

import org.apache.spark.sql.DataFrame;
import org.eocencle.magnet.component.WorkStageOperate;
import org.eocencle.magnet.mapping.FilterInfo;

import java.util.List;

/**
 * Spark过滤条件接口
 * @author: huan
 * @Date: 2020-03-12
 * @Description:
 */
public interface SparkFilterCondition extends WorkStageOperate {
    /**
     * 过滤验证
     * @Author huan
     * @Date 2020-03-12
     * @Param [filterFields, df]
     * @Return org.apache.spark.sql.DataFrame
     * @Exception
     * @Description
     **/
    DataFrame filter(List<FilterInfo.FilterField> filterFields, DataFrame df);
}
