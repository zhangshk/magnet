package org.eocencle.magnet.component.spark;

import org.apache.spark.sql.DataFrame;
import org.eocencle.magnet.mapping.ValueMappersInfo;

/**
 * Spark默认值映射类
 * @author: huan
 * @Date: 2020-04-08
 * @Description:
 */
public class SparkDefaultValueMappersHandler implements SparkValueMappersHandler {
    @Override
    public DataFrame handle(DataFrame df, ValueMappersInfo valueMappersInfo) {

        return null;
    }
}
