package org.eocencle.magnet.component.spark;

import org.eocencle.magnet.component.ValueMappersWorkStage;
import org.eocencle.magnet.component.WorkStageOperate;
import org.eocencle.magnet.component.WorkStageParameter;
import org.eocencle.magnet.component.WorkStageResult;

import java.util.List;

/**
 * Spark值映射作业节点类
 * @author: huan
 * @Date: 2020-04-08
 * @Description:
 */
public class SparkValueMappersWorkStage extends ValueMappersWorkStage {

    @Override
    public void initOperate(WorkStageOperate operate) {

    }

    @Override
    public List<WorkStageResult> execute(WorkStageParameter parameter) {
        return null;
    }
}
