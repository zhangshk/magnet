package org.eocencle.magnet.component.spark;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.eocencle.magnet.component.*;
import org.eocencle.magnet.context.factory.RuntimeEnvironmentFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Spark关联作业节点类
 * @author: huan
 * @Date: 2020-04-04
 * @Description:
 */
public class SparkJoinWorkStage extends JoinWorkStage {
    // on条件
    private SparkJoinCondition sparkJoinCondition;

    @Override
    public void initOperate(WorkStageOperate operate) {
        this.sparkJoinCondition = (SparkJoinCondition) operate;
    }

    @Override
    public List<WorkStageResult> execute(WorkStageParameter parameter) {
        RuntimeEnvironmentFactory factory = WorkStageComponentBuilderAssistant.getFactory();

        WorkStageComposite parent = this.getParent();
        DataFrame left = ((SparkWorkStageResult) parent.getIdResult(this.joinInfo.getLeftRef())).getDf();
        DataFrame right = ((SparkWorkStageResult) parent.getIdResult(this.joinInfo.getRightRef())).getDf();

        // 创建DataFrame
        DataFrame df = this.sparkJoinCondition.on(this.joinInfo.getFilterFields(), left, right, this.joinInfo.getType());

        // 创建RDD
        JavaRDD<Row> rdd = df.toJavaRDD();

        // 设置返回值
        SparkWorkStageResult result = (SparkWorkStageResult) factory.createWorkStageResult();
        result.setId(this.joinInfo.getId());
        result.setAlias(this.joinInfo.getAlias());
        result.setRdd(rdd);
        result.setDf(df);
        List<WorkStageResult> list = new ArrayList<>();
        list.add(result);
        return list;
    }
}
