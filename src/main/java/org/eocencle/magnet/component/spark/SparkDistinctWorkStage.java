package org.eocencle.magnet.component.spark;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.eocencle.magnet.component.*;
import org.eocencle.magnet.context.factory.RuntimeEnvironmentFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Spark去重作业节点类
 * @author: huan
 * @Date: 2020-03-15
 * @Description:
 */
public class SparkDistinctWorkStage extends DistinctWorkStage {

    @Override
    public void initOperate(WorkStageOperate operate) {

    }

    @Override
    public List<WorkStageResult> execute(WorkStageParameter parameter) {
        RuntimeEnvironmentFactory factory = WorkStageComponentBuilderAssistant.getFactory();

        SparkWorkStageResult prevResult =
                (SparkWorkStageResult) this.getParent().getPrevResult(this.distinctInfo.getRef());

        // 创建DataFrame
        DataFrame df = prevResult.getDf().distinct();
        // 创建RDD
        JavaRDD<Row> rdd = df.toJavaRDD();

        // 设置返回值
        SparkWorkStageResult result = (SparkWorkStageResult) factory.createWorkStageResult();
        result.setId(this.distinctInfo.getId());
        result.setAlias(this.distinctInfo.getAlias());
        result.setRdd(rdd);
        result.setDf(df);
        List<WorkStageResult> list = new ArrayList<>();
        list.add(result);
        return list;
    }
}
