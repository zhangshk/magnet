package org.eocencle.magnet.component.spark;

import org.apache.commons.lang3.StringUtils;
import org.apache.spark.sql.DataFrameWriter;
import org.apache.spark.sql.SaveMode;
import org.eocencle.magnet.builder.Tag;
import org.eocencle.magnet.component.WorkStageResult;
import org.eocencle.magnet.exception.UnsupportedException;
import org.eocencle.magnet.mapping.OutputInfo;

import java.util.Properties;

/**
 * Spark数据库输出器类
 * @author: huan
 * @Date: 2020-03-28
 * @Description:
 */
public class SparkDataBaseOutputer implements SparkOutputer {
    @Override
    public WorkStageResult output(OutputInfo outputInfo, SparkWorkStageResult result) {
        // 构造用户信息
        Properties properties = new Properties();
        properties.put("user", outputInfo.getOutputConfig(Tag.DB_USERNAME));
        properties.put("password", outputInfo.getOutputConfig(Tag.DB_PASSWORD));

        DataFrameWriter writer = result.getDf().write();

        if (Tag.OUTPUT_TYPE_CREATE.equalsIgnoreCase(outputInfo.getType())) {
            writer = writer.mode(SaveMode.ErrorIfExists);
        } else if (Tag.OUTPUT_TYPE_OVERRIDE.equalsIgnoreCase(outputInfo.getType())) {
            writer = writer.mode(SaveMode.Overwrite);
        } else if (Tag.OUTPUT_TYPE_APPEND.equalsIgnoreCase(outputInfo.getType())) {
            writer = writer.mode(SaveMode.Append);
        }

        writer.jdbc(this.getDBUrl(outputInfo), outputInfo.getTarget(), properties);
        return null;
    }

    /**
     * 获取数据库URL
     * @Author huan
     * @Date 2020-03-28
     * @Param [outputInfo]
     * @Return java.lang.String
     * @Exception
     * @Description
     **/
    private String getDBUrl(OutputInfo outputInfo) {
        String type = outputInfo.getOutputConfig(Tag.DB_TYPE);
        String host = outputInfo.getOutputConfig(Tag.DB_HOST);
        String port = outputInfo.getOutputConfig(Tag.DB_PORT);
        String dbName = outputInfo.getOutputConfig(Tag.DB_DATABASE);
        if (Tag.DB_TYPE_MYSQL.equalsIgnoreCase(type)) {
            if (StringUtils.isBlank(port)) {
                port = Tag.DB_PORT_MYSQL;
            }
            return "jdbc:mysql://" + host + ":" + port + "/" + dbName;
        } else if (Tag.DB_TYPE_SQLSERVER.equalsIgnoreCase(type)) {
            if (StringUtils.isBlank(port)) {
                port = Tag.DB_PORT_SQLSERVER;
            }
            return "jdbc:sqlserver://" + host + ":" + port + ";DatabaseName=" + dbName;
        } else if (Tag.DB_TYPE_ORACLE.equalsIgnoreCase(type)) {
            if (StringUtils.isBlank(port)) {
                port = Tag.DB_PORT_ORACLE;
            }
            return "jdbc:oracle:thin:@" + host + ":" + port + ":" + dbName;
        }
        throw new UnsupportedException(type + " database type is not supported!");
    }
}
