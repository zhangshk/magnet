package org.eocencle.magnet.component.spark;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.eocencle.magnet.component.*;
import org.eocencle.magnet.context.Context;
import org.eocencle.magnet.context.factory.RuntimeEnvironmentFactory;
import org.eocencle.magnet.mapping.TableInfo;
import org.eocencle.magnet.mapping.WorkStageInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Spark表作业节点类
 * @author: huan
 * @Date: 2020-02-01
 * @Description:
 */
public class SparkTableWorkStage extends TableWorkStage {
    // 表加载器
    private SparkTableLoader tableLoader;

    @Override
    public void initData(WorkStageInfo info) {
        this.tableInfo = (TableInfo) info;
    }

    @Override
    public void initOperate(WorkStageOperate operate) {
        this.tableLoader = (SparkTableLoader) operate;
    }

    @Override
    public List<WorkStageResult> execute(WorkStageParameter parameter) {
        RuntimeEnvironmentFactory factory = WorkStageComponentBuilderAssistant.getFactory();
        Context context = parameter.getContext();

        JavaRDD<Row> rdd = null;
        DataFrame df = null;
        if (this.tableLoader instanceof SparkTableRDDLoader) {
            SparkTableRDDLoader loader = (SparkTableRDDLoader) this.tableLoader;
            // 创建RDD
            rdd = loader.createRDD((JavaSparkContext) context.getContext(), this.tableInfo.getSrc());
            // 创建DataFrame
            df = loader.createDataFrame((SQLContext) context.getSQLContext(), this.tableInfo.getFields(), rdd);
        } else {
            SparkTableDataFrameLoader loader = (SparkTableDataFrameLoader) this.tableLoader;
            // 创建DataFrame
            df = loader.createDataFrame(context, this.tableInfo.getSrc());
            // 创建RDD
            rdd = loader.createRDD(df);
        }

        // 设置返回值
        SparkWorkStageResult result = (SparkWorkStageResult) factory.createWorkStageResult();
        result.setId(this.tableInfo.getId());
        result.setAlias(this.tableInfo.getAlias());
        result.setRdd(rdd);
        result.setDf(df);
        List<WorkStageResult> list = new ArrayList<>();
        list.add(result);
        return list;
    }
}
