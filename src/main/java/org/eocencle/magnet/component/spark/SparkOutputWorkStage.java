package org.eocencle.magnet.component.spark;

import org.eocencle.magnet.component.OutputWorkStage;
import org.eocencle.magnet.component.WorkStageOperate;
import org.eocencle.magnet.component.WorkStageParameter;
import org.eocencle.magnet.component.WorkStageResult;
import org.eocencle.magnet.mapping.OutputInfo;
import org.eocencle.magnet.mapping.WorkStageInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Spark输出作业节点类
 * @author: huan
 * @Date: 2020-02-01
 * @Description:
 */
public class SparkOutputWorkStage extends OutputWorkStage {
    // 输出器
    private SparkOutputer outputer;

    @Override
    public void initData(WorkStageInfo info) {
        this.outputInfo = (OutputInfo) info;
    }

    @Override
    public void initOperate(WorkStageOperate operate) {
        this.outputer = (SparkOutputer) operate;
    }
    
    @Override
    public List<WorkStageResult> execute(WorkStageParameter parameter) {
        // 获取引用结果
        SparkWorkStageResult ref =
                (SparkWorkStageResult) this.getParent().getPrevResult(this.outputInfo.getRef());

        WorkStageResult result = this.outputer.output(this.outputInfo, ref);
        if (null == result) {
            return null;
        } else {
            List<WorkStageResult> list = new ArrayList<>();
            list.add(result);
            return list;
        }
    }
}
