package org.eocencle.magnet.component.spark;

import org.apache.spark.sql.Column;
import org.apache.spark.sql.DataFrame;
import org.eocencle.magnet.builder.Tag;
import org.eocencle.magnet.mapping.FilterInfo;

import java.util.List;

/**
 * Spark默认on条件接口
 * @author: huan
 * @Date: 2020-04-05
 * @Description:
 */
public class SparkDefaultOnCondition implements SparkJoinCondition {
    @Override
    public DataFrame on(List<FilterInfo.FilterField> filterFields, DataFrame left, DataFrame right, String type) {
        Column first = left.col(filterFields.get(0).getField()).equalTo(right.col(filterFields.get(0).getValue()));
        Column column = first, tmp = null;
        FilterInfo.FilterField field = null;
        for (int i = 1; i < filterFields.size(); i ++) {
            field = filterFields.get(i);
            tmp = left.col(field.getField()).equalTo(right.col(field.getValue()));
            if (Tag.FILTER_JOIN_AND.equals(field.getType())) {
                column.and(tmp);
            } else if (Tag.FILTER_JOIN_OR.equals(field.getType())) {
                column.or(tmp);
            }
            column = tmp;
        }

        return left.join(right, first, type);
    }
}
