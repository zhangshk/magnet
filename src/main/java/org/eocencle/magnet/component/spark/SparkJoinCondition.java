package org.eocencle.magnet.component.spark;

import org.apache.spark.sql.DataFrame;
import org.eocencle.magnet.component.WorkStageOperate;
import org.eocencle.magnet.mapping.FilterInfo;

import java.util.List;

/**
 * Spark关联条件接口
 * @author: huan
 * @Date: 2020-04-05
 * @Description:
 */
public interface SparkJoinCondition extends WorkStageOperate {
    /**
     * 关联条件
     * @Author huan
     * @Date 2020-04-05
     * @Param [filterFields, left, right, type]
     * @Return org.apache.spark.sql.DataFrame
     * @Exception
     * @Description
     **/
    DataFrame on(List<FilterInfo.FilterField> filterFields, DataFrame left, DataFrame right, String type);
}
