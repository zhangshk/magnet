package org.eocencle.magnet.component.spark;

import kafka.serializer.StringDecoder;
import org.apache.commons.lang3.StringUtils;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.types.*;
import org.apache.spark.streaming.api.java.JavaPairInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka.KafkaUtils;
import org.eocencle.magnet.builder.Tag;
import org.eocencle.magnet.component.*;
import org.eocencle.magnet.context.Context;
import org.eocencle.magnet.context.factory.RuntimeEnvironmentFactory;
import org.eocencle.magnet.mapping.DataSourceField;
import org.eocencle.magnet.mapping.InfoParam;
import org.eocencle.magnet.mapping.StreamInfo;
import org.eocencle.magnet.mapping.WorkStageInfo;
import org.eocencle.magnet.util.StrictMap;
import scala.Tuple2;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * spark流作业节点类
 * @author: huan
 * @Date: 2020-02-02
 * @Description:
 */
public class SparkStreamWorkStage extends StreamWorkStage implements Serializable {
    // sparkSQL
    private SQLContext sqlContext;

    @Override
    public void initData(WorkStageInfo info) {
        this.streamInfo = (StreamInfo) info;
    }

    @Override
    public void initOperate(WorkStageOperate operate) {

    }

    @Override
    public List<WorkStageResult> execute(WorkStageParameter parameter) {
        Context context = parameter.getContext();
        this.sqlContext = (SQLContext) context.getSQLContext();
        JavaStreamingContext streamingContext = (JavaStreamingContext) context.getStreamContext();

        // 主题
        Set<String> topics = new HashSet<>(Arrays.asList(this.streamInfo.getTopics().split(Tag.STRING_COMMA)));

        // kafka参数
        StrictMap<String> kafkaConfig = new StrictMap<String>("kafka config");
        InfoParam param = null;
        for (Map.Entry<String, InfoParam> entry: this.streamInfo.getKafkaConfig().entrySet()) {
            param = entry.getValue();
            kafkaConfig.put(param.getKey(), param.getValue());
        }

        // 创建流
        JavaPairInputDStream<String, String> dStream = KafkaUtils.createDirectStream(streamingContext, String.class,
                String.class, StringDecoder.class, StringDecoder.class, kafkaConfig, topics);

        // 遍历流
        dStream.map((Tuple2<String, String> tuple2) -> tuple2._2).foreachRDD((JavaRDD<String> line) -> {
            RuntimeEnvironmentFactory factory = WorkStageComponentBuilderAssistant.getFactory();

            // 创建RDD
            JavaRDD<Row> rdd = this.createRDD(line, streamInfo);
            // 创建DataFrame
            DataFrame df = this.createDataFrame(streamInfo.getFields(), rdd);

            SparkWorkStageResult result = (SparkWorkStageResult) factory.createWorkStageResult();
            result.setId(streamInfo.getId());
            result.setAlias(streamInfo.getAlias());
            result.setRdd(rdd);
            result.setDf(df);

            WorkStageComposite parent = getParent();
            String id = streamInfo.getId();
            String idName = parent.getMixedTableName(id);
            parent.putTableName(id, idName);

            df.registerTempTable(idName);

            parent.changeLastResult(result);

            for (WorkStageComponent component: components) {
                component.execute(parameter);
            }
        });

        return null;
    }

    @Override
    public void add(WorkStageComponent component) {
        this.components.add(component);
        component.setParent(this.getParent());
    }

    /**
     * 创建RDD
     * @Author huan
     * @Date 2020-03-19
     * @Param [lines, info]
     * @Return org.apache.spark.api.java.JavaRDD<org.apache.spark.sql.Row>
     * @Exception
     * @Description
     **/
    private JavaRDD<Row> createRDD(JavaRDD<String> lines, StreamInfo info) {
        return lines.map((String line) -> {
            String[] row = line.split(info.getSeparator(), -1);
            StrictMap<DataSourceField> dst = info.getFields();
            Object[] fields = new Object[dst.size()];
            int i = 0;
            String type = null;
            SimpleDateFormat tagSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            for (Map.Entry<String, DataSourceField> entry: dst.entrySet()) {
                type = entry.getValue().getType();

                if (Tag.TABLE_FIELD_TYPE_STRING.equalsIgnoreCase(type)) {
                    fields[i] = row[i];
                } else if (Tag.TABLE_FIELD_TYPE_BOOLEAN.equalsIgnoreCase(type)
                        || Tag.TABLE_FIELD_TYPE_BOOL.equalsIgnoreCase(type)) {
                    if (StringUtils.isNotBlank(row[i])) {
                        fields[i] = Boolean.valueOf(row[i]);
                    } else {
                        fields[i] = null;
                    }
                } else if (Tag.TABLE_FIELD_TYPE_DATETIME.equalsIgnoreCase(type)) {
                    if (StringUtils.isNotBlank(row[i])) {
                        SimpleDateFormat srcSdf = new SimpleDateFormat(entry.getValue().getFormat());
                        fields[i] = Timestamp.valueOf(tagSdf.format(srcSdf.parse(row[i])));
                    } else {
                        fields[i] = null;
                    }
                } else if (Tag.TABLE_FIELD_TYPE_DOUBLE.equalsIgnoreCase(type)) {
                    if (StringUtils.isNotBlank(row[i])) {
                        fields[i] = Double.valueOf(row[i]);
                    } else {
                        fields[i] = null;
                    }
                } else if (Tag.TABLE_FIELD_TYPE_FLOAT.equalsIgnoreCase(type)) {
                    if (StringUtils.isNotBlank(row[i])) {
                        fields[i] = Float.valueOf(row[i]);
                    } else {
                        fields[i] = null;
                    }
                } else if (Tag.TABLE_FIELD_TYPE_BYTE.equalsIgnoreCase(type)) {
                    if (StringUtils.isNotBlank(row[i])) {
                        fields[i] = Byte.valueOf(row[i]);
                    } else {
                        fields[i] = null;
                    }
                } else if (Tag.TABLE_FIELD_TYPE_INTEGER.equalsIgnoreCase(type)
                        || Tag.TABLE_FIELD_TYPE_INT.equalsIgnoreCase(type)) {
                    if (StringUtils.isNotBlank(row[i])) {
                        fields[i] = Integer.valueOf(row[i]);
                    } else {
                        fields[i] = null;
                    }
                } else if (Tag.TABLE_FIELD_TYPE_LONG.equalsIgnoreCase(type)) {
                    if (StringUtils.isNotBlank(row[i])) {
                        fields[i] = Long.valueOf(row[i]);
                    } else {
                        fields[i] = null;
                    }
                } else if (Tag.TABLE_FIELD_TYPE_SHORT.equalsIgnoreCase(type)) {
                    if (StringUtils.isNotBlank(row[i])) {
                        fields[i] = Short.valueOf(row[i]);
                    } else {
                        fields[i] = null;
                    }
                } else if (Tag.TABLE_FIELD_TYPE_DECIMAL.equalsIgnoreCase(type)) {
                    if (StringUtils.isNotBlank(row[i])) {
                        fields[i] = Decimal.apply(row[i]);
                    } else {
                        fields[i] = null;
                    }
                } else {
                    fields[i] = row[i];
                }

                i ++;
            }
            return RowFactory.create(fields);
        });
    }

    /**
     * 创建DataFrame
     * @Author huan
     * @Date 2020-03-18
     * @Param [dst, rdd]
     * @Return org.apache.spark.sql.DataFrame
     * @Exception
     * @Description
     **/
    private DataFrame createDataFrame(StrictMap<DataSourceField> dst, JavaRDD<Row> rdd) {
        List<StructField> structFields = new ArrayList<StructField>();
        String name = null, type = null;
        DataType fieldType = null;
        DataSourceField field = null;
        for (Map.Entry<String, DataSourceField> entry: dst.entrySet()) {
            field = entry.getValue();
            name = field.getName();
            type = field.getType();

            if (Tag.TABLE_FIELD_TYPE_STRING.equalsIgnoreCase(type)) {
                fieldType = DataTypes.StringType;
            } else if (Tag.TABLE_FIELD_TYPE_BINARY.equalsIgnoreCase(type)) {
                fieldType = DataTypes.BinaryType;
            } else if (Tag.TABLE_FIELD_TYPE_BOOLEAN.equalsIgnoreCase(type)
                    || Tag.TABLE_FIELD_TYPE_BOOL.equalsIgnoreCase(type)) {
                fieldType = DataTypes.BooleanType;
            } else if (Tag.TABLE_FIELD_TYPE_DATETIME.equalsIgnoreCase(type) ||
                    Tag.TABLE_FIELD_TYPE_TIMESTAMP.equalsIgnoreCase(type)) {
                fieldType = DataTypes.TimestampType;
            } else if (Tag.TABLE_FIELD_TYPE_CALENDARINTERVAL.equalsIgnoreCase(type)) {
                fieldType = DataTypes.CalendarIntervalType;
            } else if (Tag.TABLE_FIELD_TYPE_DOUBLE.equalsIgnoreCase(type)) {
                fieldType = DataTypes.DoubleType;
            } else if (Tag.TABLE_FIELD_TYPE_FLOAT.equalsIgnoreCase(type)) {
                fieldType = DataTypes.FloatType;
            } else if (Tag.TABLE_FIELD_TYPE_BYTE.equalsIgnoreCase(type)) {
                fieldType = DataTypes.ByteType;
            } else if (Tag.TABLE_FIELD_TYPE_INTEGER.equalsIgnoreCase(type)
                    || Tag.TABLE_FIELD_TYPE_INT.equalsIgnoreCase(type)) {
                fieldType = DataTypes.IntegerType;
            } else if (Tag.TABLE_FIELD_TYPE_LONG.equalsIgnoreCase(type)) {
                fieldType = DataTypes.LongType;
            } else if (Tag.TABLE_FIELD_TYPE_SHORT.equalsIgnoreCase(type)) {
                fieldType = DataTypes.ShortType;
            } else if (Tag.TABLE_FIELD_TYPE_DECIMAL.equalsIgnoreCase(type)) {
                if (StringUtils.isNotBlank(field.getPrecision())) {
                    String[] precision = field.getPrecision().split(Tag.SPLIT_COMMA);
                    if (2 == precision.length) {
                        fieldType = DataTypes.createDecimalType(Integer.parseInt(precision[0]),Integer.parseInt(precision[1]));
                    } else {
                        fieldType = DataTypes.createDecimalType();
                    }
                } else {
                    fieldType = DataTypes.createDecimalType();
                }
            } else {
                fieldType = DataTypes.NullType;
            }

            structFields.add(DataTypes.createStructField(name, fieldType, true));
        }
        StructType structType = DataTypes.createStructType(structFields);
        return this.sqlContext.createDataFrame(rdd, structType);
    }
}
