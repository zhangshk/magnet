package org.eocencle.magnet.component.spark;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.eocencle.magnet.builder.Tag;
import org.eocencle.magnet.component.*;
import org.eocencle.magnet.context.factory.RuntimeEnvironmentFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Spark合并作业节点类
 * @author: huan
 * @Date: 2020-04-03
 * @Description:
 */
public class SparkUnionWorkStage extends UnionWorkStage {

    @Override
    public void initOperate(WorkStageOperate operate) {

    }

    @Override
    public List<WorkStageResult> execute(WorkStageParameter parameter) {
        RuntimeEnvironmentFactory factory = WorkStageComponentBuilderAssistant.getFactory();

        String[] refs = this.unionInfo.getRefs().split(Tag.SPLIT_COMMA);
        WorkStageComposite parent = this.getParent();

        // 创建DataFrame
        DataFrame df = ((SparkWorkStageResult) parent.getIdResult(refs[0])).getDf();

        for (int i = 1; i < refs.length; i ++) {
            df = df.unionAll(((SparkWorkStageResult) parent.getIdResult(refs[i])).getDf());
        }

        // 创建RDD
        JavaRDD<Row> rdd = df.toJavaRDD();

        // 设置返回值
        SparkWorkStageResult result = (SparkWorkStageResult) factory.createWorkStageResult();
        result.setId(this.unionInfo.getId());
        result.setAlias(this.unionInfo.getAlias());
        result.setRdd(rdd);
        result.setDf(df);
        List<WorkStageResult> list = new ArrayList<>();
        list.add(result);
        return list;
    }
}
