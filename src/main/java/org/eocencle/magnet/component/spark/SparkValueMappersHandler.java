package org.eocencle.magnet.component.spark;

import org.apache.spark.sql.DataFrame;
import org.eocencle.magnet.component.WorkStageOperate;
import org.eocencle.magnet.mapping.ValueMappersInfo;

/**
 * Spark值映射器
 * @author: huan
 * @Date: 2020-04-08
 * @Description:
 */
public interface SparkValueMappersHandler extends WorkStageOperate {
    /**
     * 值映射处理
     * @Author huan
     * @Date 2020-04-08
     * @Param [df, valueMappersInfo]
     * @Return org.apache.spark.sql.DataFrame
     * @Exception
     * @Description
     **/
    DataFrame handle(DataFrame df, ValueMappersInfo valueMappersInfo);

}
