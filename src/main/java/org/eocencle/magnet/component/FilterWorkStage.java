package org.eocencle.magnet.component;

import org.eocencle.magnet.mapping.FilterInfo;

/**
 * 过滤作业节点抽象类
 * @author: huan
 * @Date: 2020-03-12
 * @Description:
 */
public abstract class FilterWorkStage extends WorkStageComponent {
    // 过滤信息
    protected FilterInfo filterInfo;
}
