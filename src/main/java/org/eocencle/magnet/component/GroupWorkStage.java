package org.eocencle.magnet.component;

import org.eocencle.magnet.mapping.GroupInfo;

/**
 * 组作业节点抽象类
 * @author: huan
 * @Date: 2020-01-12
 * @Description:
 */
public abstract class GroupWorkStage extends WorkStageComponent {
    // 分组信息
    protected GroupInfo groupInfo;
}
