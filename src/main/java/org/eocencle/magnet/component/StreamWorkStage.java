package org.eocencle.magnet.component;

import org.eocencle.magnet.mapping.StreamInfo;

/**
 * 流作业节点抽象类
 * @author: huan
 * @Date: 2020-01-12
 * @Description:
 */
public abstract class StreamWorkStage extends WorkStageComponent {
    // 流信息
    protected StreamInfo streamInfo;
}
