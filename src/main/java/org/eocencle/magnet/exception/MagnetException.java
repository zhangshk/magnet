package org.eocencle.magnet.exception;

/**
 * Magnet框架异常类
 * @author: huan
 * @Date: 2020-03-29
 * @Description:
 */
public class MagnetException extends Exception {
    public MagnetException(String message) {
        super(message);
    }
}
