package org.eocencle.magnet.context.spark;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.eocencle.magnet.builder.Tag;
import org.eocencle.magnet.context.Context;
import org.eocencle.magnet.exception.UnsupportedException;

/**
 * Spark1.6.0版执行环境
 * @author: huan
 * @Date: 2020-01-12
 * @Description:
 */
public class Spark1_6_0Context extends Context {
    // spark配置
    private SparkConf sparkConf;
    // spark上下文
    private JavaSparkContext sc;
    // sparkSQL上下文
    private SQLContext sqlContext;
    // sparkStream上下文
    private JavaStreamingContext ssc;

    @Override
    public void start() {
        this.sparkConf = new SparkConf().setAppName(this.getAppName());
        this.sparkConf.set("spark.extraListeners", "org.eocencle.magnet.listener.TestListener");
        // 环境模式判断
        if (this.getEnvMode().toUpperCase().startsWith(Tag.ENV_MODE_LOCAL.toUpperCase())) {
            this.sparkConf.setMaster(this.getEnvMode().toLowerCase());
        }

        // 执行模式判断
        if (Tag.PROCESS_MODE_BATCH.equalsIgnoreCase(this.getProcessMode())) {
            this.sc = new JavaSparkContext(this.sparkConf);
            if (Tag.SQL_ENGINE_SPARK.equalsIgnoreCase(this.getSqlEngine())) {
                this.sqlContext = new SQLContext(this.sc);
            } else if (Tag.SQL_ENGINE_HIVE.equalsIgnoreCase(this.getSqlEngine())) {

            } else {
                throw new UnsupportedException(this.getSqlEngine() + " SQL engine not supported");
            }
        } else if (Tag.PROCESS_MODE_STREAM.equalsIgnoreCase(this.getProcessMode())) {
            this.ssc = new JavaStreamingContext(this.sparkConf, Durations.seconds(this.getDuration()));
            this.sc = this.ssc.sparkContext();
            if (Tag.SQL_ENGINE_SPARK.equalsIgnoreCase(this.getSqlEngine())) {
                this.sqlContext = new SQLContext(this.ssc.sparkContext());
            } else if (Tag.SQL_ENGINE_HIVE.equalsIgnoreCase(this.getSqlEngine())) {

            } else {
                throw new UnsupportedException(this.getSqlEngine() + " SQL engine not supported");
            }
        }
    }

    @Override
    public void stop() {
        if (Tag.PROCESS_MODE_BATCH.equalsIgnoreCase(this.getProcessMode())) {
            this.sc.stop();
        } else if (Tag.PROCESS_MODE_STREAM.equalsIgnoreCase(this.getProcessMode())) {
            this.ssc.start();
            this.ssc.awaitTermination();
            this.ssc.close();
        }
    }

    @Override
    public Object getContext() {
        return this.sc;
    }

    @Override
    public Object getSQLContext() {
        return this.sqlContext;
    }

    @Override
    public Object getStreamContext() {
        return this.ssc;
    }
}
