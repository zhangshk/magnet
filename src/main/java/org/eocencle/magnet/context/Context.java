package org.eocencle.magnet.context;

/**
 * 上下文环境抽象类
 * @author: huan
 * @Date: 2020-01-12
 * @Description:
 */
public abstract class Context {
    // 应用名称
    private String appName;
    // 环境模式
    private String envMode;
    // 处理模式
    private String processMode;
    // 持续时间
    private Integer duration;
    // SQL引擎
    private String sqlEngine;

    /**
     * 启动环境
     * @Author huan
     * @Date 2020-01-22
     * @Param []
     * @Return void
     * @Exception
     * @Description
     **/
    public abstract void start();

    /**
     * 停止环境
     * @Author huan
     * @Date 2020-01-22
     * @Param []
     * @Return void
     * @Exception
     * @Description
     **/
    public abstract void stop();

    /**
     * 获取上下文
     * @Author huan
     * @Date 2020-01-21
     * @Param []
     * @Return java.lang.Object
     * @Exception
     * @Description
     **/
    public abstract Object getContext();

    /**
     * 获取SQL上下文
     * @Author huan
     * @Date 2020-01-21
     * @Param []
     * @Return java.lang.Object
     * @Exception
     * @Description
     **/
    public abstract Object getSQLContext();

    /**
     * 获取流上下文
     * @Author huan
     * @Date 2020-02-15
     * @Param []
     * @Return java.lang.Object
     * @Exception
     * @Description
     **/
    public abstract Object getStreamContext();

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getEnvMode() {
        return envMode;
    }

    public void setEnvMode(String envMode) {
        this.envMode = envMode;
    }

    public String getProcessMode() {
        return processMode;
    }

    public void setProcessMode(String processMode) {
        this.processMode = processMode;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getSqlEngine() {
        return sqlEngine;
    }

    public void setSqlEngine(String sqlEngine) {
        this.sqlEngine = sqlEngine;
    }
}
