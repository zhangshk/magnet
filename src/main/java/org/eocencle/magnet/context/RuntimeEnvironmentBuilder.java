package org.eocencle.magnet.context;

import org.eocencle.magnet.builder.Tag;
import org.eocencle.magnet.context.factory.FlinkRuntimeEnvironmentFactory;
import org.eocencle.magnet.context.factory.RuntimeEnvironmentFactory;
import org.eocencle.magnet.context.factory.SparkRuntimeEnvironmentFactory;
import org.eocencle.magnet.exception.UnsupportedException;
import org.eocencle.magnet.session.RuntimeEnvironmentConfig;

/**
 * 运行时环境建构类
 * @author: huan
 * @Date: 2020-01-12
 * @Description:
 */
public class RuntimeEnvironmentBuilder {
    /**
     * 构造环境工厂
     * @Author huan
     * @Date 2020-02-02
     * @Param [config]
     * @Return org.eocencle.magnet.context.factory.RuntimeEnvironmentFactory
     * @Exception
     * @Description
     **/
    public static RuntimeEnvironmentFactory construct(RuntimeEnvironmentConfig config) {
        if (Tag.CONTEXT_SPARK.equalsIgnoreCase(config.getRunEnv())) {
            return SparkRuntimeEnvironmentFactory.getFactoryInstance();
        } else if (Tag.CONTEXT_FLINK.equalsIgnoreCase(config.getRunEnv())) {
            return FlinkRuntimeEnvironmentFactory.getFactoryInstance();
        }
        throw new UnsupportedException(Tag.MAGNET_PROCESS_MODE + " does not support " + config.getRunEnv() + " mode!");
    }
}
