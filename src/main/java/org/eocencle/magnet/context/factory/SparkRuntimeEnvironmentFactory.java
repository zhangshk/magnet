package org.eocencle.magnet.context.factory;

import org.eocencle.magnet.builder.Tag;
import org.eocencle.magnet.component.*;
import org.eocencle.magnet.component.spark.*;
import org.eocencle.magnet.context.Context;
import org.eocencle.magnet.context.spark.Spark1_6_0Context;
import org.eocencle.magnet.exception.UnsupportedException;
import org.eocencle.magnet.mapping.*;

/**
 * Spark环境工厂类
 * @author: huan
 * @Date: 2020-01-12
 * @Description:
 */
public class SparkRuntimeEnvironmentFactory implements RuntimeEnvironmentFactory {

    private static SparkRuntimeEnvironmentFactory FACTORY = new SparkRuntimeEnvironmentFactory();

    private SparkRuntimeEnvironmentFactory() {

    }

    /**
     * 单例获取工厂实例
     * @Author huan
     * @Date 2020-01-22
     * @Param []
     * @Return org.eocencle.magnet.context.factory.SparkRuntimeEnvironmentFactory
     * @Exception
     * @Description
     **/
    public static SparkRuntimeEnvironmentFactory getFactoryInstance() {
        return FACTORY;
    }

    @Override
    public Context getRuntimeContext() {
        return new Spark1_6_0Context();
    }

    @Override
    public TableWorkStage createTableWorkStageComponent() {
        return new SparkTableWorkStage();
    }

    @Override
    public StreamWorkStage createStreamWorkStageComponent() {
        return new SparkStreamWorkStage();
    }

    @Override
    public SQLWorkStage createSQLWorkStageComponent() {
        return new SparkSQLWorkStage();
    }

    @Override
    public GroupWorkStage createGroupWorkStageComponent() {
        return new SparkGroupWorkStage();
    }

    @Override
    public OutputWorkStage createOutputWorkStageComponent() {
        return new SparkOutputWorkStage();
    }

    @Override
    public FilterWorkStage createFilterWorkStageComponent() {
        return new SparkFilterWorkStage();
    }

    @Override
    public DistinctWorkStage createDistinctWorkStageComponent() {
        return new SparkDistinctWorkStage();
    }

    @Override
    public OrderWorkStage createOrderWorkStageComponent() {
        return new SparkOrderWorkStage();
    }

    @Override
    public UnionWorkStage createUnionWorkStageComponent() {
        return new SparkUnionWorkStage();
    }

    @Override
    public JoinWorkStage createJoinWorkStageComponent() {
        return new SparkJoinWorkStage();
    }

    @Override
    public SchemaWorkStage createSchemaWorkStageComponent() {
        return new SparkSchemaWorkStage();
    }

    @Override
    public ValueMappersWorkStage createValueMappersWorkStageComponent() {
        return new SparkValueMappersWorkStage();
    }

    @Override
    public WorkStageResult createWorkStageResult() {
        return new SparkWorkStageResult();
    }

    @Override
    public WorkStageOperate createTableWorkStageOperate(TableInfo tableInfo) {
        if (Tag.TABLE_STYLE_DATABASE.equalsIgnoreCase(tableInfo.getStyle())) {
            return new SparkDataBaseTableLoader(tableInfo);
        } else {
            if (Tag.FILE_FORMAT_TEXTFILE.equalsIgnoreCase(tableInfo.getFormat())) {
                return new SparkTextTableLoader(tableInfo);
            } else if (Tag.FILE_FORMAT_AVROFILE.equalsIgnoreCase(tableInfo.getFormat())) {
                return new SparkAvroTableLoader(tableInfo);
            } else if (Tag.FILE_FORMAT_RCFILE.equalsIgnoreCase(tableInfo.getFormat())) {
                return new SparkRCTableLoader(tableInfo);
            } else if (Tag.FILE_FORMAT_ORCFILE.equalsIgnoreCase(tableInfo.getFormat())) {
                return new SparkORCTableLoader(tableInfo);
            } else if (Tag.FILE_FORMAT_PARQUETFILE.equalsIgnoreCase(tableInfo.getFormat())) {
                return new SparkParquetTableLoader(tableInfo);
            }
        }
        throw new UnsupportedException("Table does not support " + tableInfo.getStyle() + " style!");
    }

    @Override
    public WorkStageOperate createStreamWorkStageOperate(StreamInfo streamInfo) {
        return null;
    }

    @Override
    public WorkStageOperate createSQLWorkStageOperate(SQLScriptInfo sqlScriptInfo) {
        return null;
    }

    @Override
    public WorkStageOperate createGroupWorkStageOperate(GroupInfo groupInfo) {
        return new SparkDefaultGrouper();
    }

    @Override
    public WorkStageOperate createOutputWorkStageOperate(OutputInfo outputInfo) {
        if (Tag.OUTPUT_STYLE_FILE.equalsIgnoreCase(outputInfo.getStyle())) {
            return new SparkFileOutputer();
        } else if (Tag.OUTPUT_STYLE_BRANCH.equalsIgnoreCase(outputInfo.getStyle())) {
            return new SparkBranchOutputer();
        } else if (Tag.OUTPUT_STYLE_EMAIL.equalsIgnoreCase(outputInfo.getStyle())) {
            return new SparkEmailOutputer();
        } else if (Tag.OUTPUT_STYLE_CONSOLE.equalsIgnoreCase(outputInfo.getStyle())) {
            return new SparkConsoleOutputer();
        } else if (Tag.TABLE_STYLE_DATABASE.equalsIgnoreCase(outputInfo.getStyle())) {
            return new SparkDataBaseOutputer();
        } else if (Tag.TABLE_STYLE_KAFKA.equalsIgnoreCase(outputInfo.getStyle())) {
            return new SparkKafkaOutputer();
        }
        throw new UnsupportedException("Output does not support " + outputInfo.getStyle() + " style!");
    }

    @Override
    public WorkStageOperate createFilterWorkStageOperate(FilterInfo filterInfo) {
        return new SparkDefaultFilterCondition();
    }

    @Override
    public WorkStageOperate createOrderWorkStageOperate(OrderInfo orderInfo) {
        return new SparkDefaultOrder();
    }

    @Override
    public WorkStageOperate createJoinWorkStageOperate(JoinInfo joinInfo) {
        return new SparkDefaultOnCondition();
    }

    @Override
    public WorkStageOperate createValueMappersWorkStageOperate(ValueMappersInfo valueMappersInfo) {
        return null;
    }
}
