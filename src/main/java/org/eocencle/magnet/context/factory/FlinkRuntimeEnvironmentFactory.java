package org.eocencle.magnet.context.factory;

import org.eocencle.magnet.component.*;
import org.eocencle.magnet.context.Context;
import org.eocencle.magnet.mapping.*;

/**
 * Flink环境工厂类
 * @author: huan
 * @Date: 2020-01-12
 * @Description:
 */
public class FlinkRuntimeEnvironmentFactory implements RuntimeEnvironmentFactory {

    private static FlinkRuntimeEnvironmentFactory FACTORY = new FlinkRuntimeEnvironmentFactory();

    private FlinkRuntimeEnvironmentFactory() {

    }

    /**
     * 单例获取工厂实例
     * @Author huan
     * @Date 2020-01-22
     * @Param []
     * @Return org.eocencle.magnet.context.factory.FlinkRuntimeEnvironmentFactory
     * @Exception
     * @Description
     **/
    public static FlinkRuntimeEnvironmentFactory getFactoryInstance() {
        return FACTORY;
    }

    @Override
    public Context getRuntimeContext() {
        return null;
    }

    @Override
    public TableWorkStage createTableWorkStageComponent() {
        return null;
    }

    @Override
    public StreamWorkStage createStreamWorkStageComponent() {
        return null;
    }

    @Override
    public SQLWorkStage createSQLWorkStageComponent() {
        return null;
    }

    @Override
    public GroupWorkStage createGroupWorkStageComponent() {
        return null;
    }

    @Override
    public OutputWorkStage createOutputWorkStageComponent() {
        return null;
    }

    @Override
    public FilterWorkStage createFilterWorkStageComponent() {
        return null;
    }

    @Override
    public DistinctWorkStage createDistinctWorkStageComponent() {
        return null;
    }

    @Override
    public OrderWorkStage createOrderWorkStageComponent() {
        return null;
    }

    @Override
    public UnionWorkStage createUnionWorkStageComponent() {
        return null;
    }

    @Override
    public JoinWorkStage createJoinWorkStageComponent() {
        return null;
    }

    @Override
    public SchemaWorkStage createSchemaWorkStageComponent() {
        return null;
    }

    @Override
    public ValueMappersWorkStage createValueMappersWorkStageComponent() {
        return null;
    }

    @Override
    public WorkStageResult createWorkStageResult() {
        return null;
    }

    @Override
    public WorkStageOperate createTableWorkStageOperate(TableInfo tableInfo) {
        return null;
    }

    @Override
    public WorkStageOperate createStreamWorkStageOperate(StreamInfo streamInfo) {
        return null;
    }

    @Override
    public WorkStageOperate createSQLWorkStageOperate(SQLScriptInfo sqlScriptInfo) {
        return null;
    }

    @Override
    public WorkStageOperate createGroupWorkStageOperate(GroupInfo groupInfo) {
        return null;
    }

    @Override
    public WorkStageOperate createOutputWorkStageOperate(OutputInfo outputInfo) {
        return null;
    }

    @Override
    public WorkStageOperate createFilterWorkStageOperate(FilterInfo filterInfo) {
        return null;
    }

    @Override
    public WorkStageOperate createOrderWorkStageOperate(OrderInfo orderInfo) {
        return null;
    }

    @Override
    public WorkStageOperate createJoinWorkStageOperate(JoinInfo joinInfo) {
        return null;
    }

    @Override
    public WorkStageOperate createValueMappersWorkStageOperate(ValueMappersInfo valueMappersInfo) {
        return null;
    }
}
