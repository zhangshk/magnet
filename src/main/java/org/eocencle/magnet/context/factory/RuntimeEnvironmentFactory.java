package org.eocencle.magnet.context.factory;

import org.eocencle.magnet.component.*;
import org.eocencle.magnet.context.Context;
import org.eocencle.magnet.mapping.*;

/**
 * 环境抽象工厂接口
 * @author: huan
 * @Date: 2020-01-12
 * @Description:
 */
public interface RuntimeEnvironmentFactory {
    /**
     * 获取执行环境
     * @Author huan
     * @Date 2020-01-22
     * @Param []
     * @Return org.eocencle.magnet.context.Context
     * @Exception
     * @Description
     **/
    Context getRuntimeContext();

    /**
     * 创建表作业组件
     * @Author huan
     * @Date 2020-01-22
     * @Param []
     * @Return org.eocencle.magnet.component.TableWorkStage
     * @Exception
     * @Description
     **/
    TableWorkStage createTableWorkStageComponent();

    /**
     * 创建流作业组件
     * @Author huan
     * @Date 2020-01-22
     * @Param []
     * @Return org.eocencle.magnet.component.StreamWorkStage
     * @Exception
     * @Description
     **/
    StreamWorkStage createStreamWorkStageComponent();

    /**
     * 创建SQL作业组件
     * @Author huan
     * @Date 2020-01-22
     * @Param []
     * @Return org.eocencle.magnet.component.SQLWorkStage
     * @Exception
     * @Description
     **/
    SQLWorkStage createSQLWorkStageComponent();

    /**
     * 创建组作业组件
     * @Author huan
     * @Date 2020-01-22
     * @Param []
     * @Return org.eocencle.magnet.component.GroupWorkStage
     * @Exception
     * @Description
     **/
    GroupWorkStage createGroupWorkStageComponent();

    /**
     * 创建输出作业组件
     * @Author huan
     * @Date 2020-01-22
     * @Param []
     * @Return org.eocencle.magnet.component.OutputWorkStage
     * @Exception
     * @Description
     **/
    OutputWorkStage createOutputWorkStageComponent();

    /**
     * 创建过滤作业组件
     * @Author huan
     * @Date 2020-03-13
     * @Param []
     * @Return org.eocencle.magnet.component.FilterWorkStage
     * @Exception
     * @Description
     **/
    FilterWorkStage createFilterWorkStageComponent();

    /**
     * 创建去重作业组件
     * @Author huan
     * @Date 2020-03-15
     * @Param []
     * @Return org.eocencle.magnet.component.DistinctWorkStage
     * @Exception
     * @Description
     **/
    DistinctWorkStage createDistinctWorkStageComponent();

    /**
     * 创建排序作业组件
     * @Author huan
     * @Date 2020-03-16
     * @Param []
     * @Return org.eocencle.magnet.component.OrderWorkStage
     * @Exception
     * @Description
     **/
    OrderWorkStage createOrderWorkStageComponent();

    /**
     * 创建合并作业组件
     * @Author huan
     * @Date 2020-04-03
     * @Param []
     * @Return org.eocencle.magnet.component.UnionWorkStage
     * @Exception
     * @Description
     **/
    UnionWorkStage createUnionWorkStageComponent();

    /**
     * 创建关联作业组件
     * @Author huan
     * @Date 2020-04-04
     * @Param []
     * @Return org.eocencle.magnet.component.JoinWorkStage
     * @Exception
     * @Description
     **/
    JoinWorkStage createJoinWorkStageComponent();

    /**
     * 创建Schema作业组件
     * @Author huan
     * @Date 2020-04-06
     * @Param []
     * @Return org.eocencle.magnet.component.SchemaWorkStage
     * @Exception
     * @Description
     **/
    SchemaWorkStage createSchemaWorkStageComponent();

    /**
     * 创建值映射作业组件
     * @Author huan
     * @Date 2020-04-08
     * @Param []
     * @Return org.eocencle.magnet.component.ValueMappersWorkStage
     * @Exception
     * @Description
     **/
    ValueMappersWorkStage createValueMappersWorkStageComponent();

    /**
     * 创建作业结果
     * @Author huan
     * @Date 2020-01-22
     * @Param []
     * @Return org.eocencle.magnet.component.WorkStageResult
     * @Exception
     * @Description
     **/
    WorkStageResult createWorkStageResult();

    /**
     * 创建表作业操作
     * @Author huan
     * @Date 2020-02-26
     * @Param [tableInfo]
     * @Return org.eocencle.magnet.component.WorkStageOperate
     * @Exception
     * @Description
     **/
    WorkStageOperate createTableWorkStageOperate(TableInfo tableInfo);

    /**
     * 创建表流作业操作
     * @Author huan
     * @Date 2020-02-26
     * @Param [streamInfo]
     * @Return org.eocencle.magnet.component.WorkStageOperate
     * @Exception
     * @Description
     **/
    WorkStageOperate createStreamWorkStageOperate(StreamInfo streamInfo);

    /**
     * 创建SQL作业操作
     * @Author huan
     * @Date 2020-02-26
     * @Param [sqlScriptInfo]
     * @Return org.eocencle.magnet.component.WorkStageOperate
     * @Exception
     * @Description
     **/
    WorkStageOperate createSQLWorkStageOperate(SQLScriptInfo sqlScriptInfo);

    /**
     * 创建组作业操作
     * @Author huan
     * @Date 2020-02-26
     * @Param [groupInfo]
     * @Return org.eocencle.magnet.component.WorkStageOperate
     * @Exception
     * @Description
     **/
    WorkStageOperate createGroupWorkStageOperate(GroupInfo groupInfo);

    /**
     * 创建输出作业操作
     * @Author huan
     * @Date 2020-02-26
     * @Param [outputInfo]
     * @Return org.eocencle.magnet.component.WorkStageOperate
     * @Exception
     * @Description
     **/
    WorkStageOperate createOutputWorkStageOperate(OutputInfo outputInfo);

    /**
     * 创建过滤作业操作
     * @Author huan
     * @Date 2020-03-13
     * @Param [filterInfo]
     * @Return org.eocencle.magnet.component.WorkStageOperate
     * @Exception
     * @Description
     **/
    WorkStageOperate createFilterWorkStageOperate(FilterInfo filterInfo);

    /**
     * 创建排序作业操作
     * @Author huan
     * @Date 2020-03-16
     * @Param [orderInfo]
     * @Return org.eocencle.magnet.component.WorkStageOperate
     * @Exception
     * @Description
     **/
    WorkStageOperate createOrderWorkStageOperate(OrderInfo orderInfo);

    /**
     * 创建连接作业操作
     * @Author huan
     * @Date 2020-04-05
     * @Param [joinInfo]
     * @Return org.eocencle.magnet.component.WorkStageOperate
     * @Exception
     * @Description
     **/
    WorkStageOperate createJoinWorkStageOperate(JoinInfo joinInfo);

    /**
     * 创建值映射作业操作
     * @Author huan
     * @Date 2020-04-08
     * @Param [valueMappersInfo]
     * @Return org.eocencle.magnet.component.WorkStageOperate
     * @Exception
     * @Description
     **/
    WorkStageOperate createValueMappersWorkStageOperate(ValueMappersInfo valueMappersInfo);
}
